'''
Created on Apr 11, 2017
Adaptation of the original java version for Python 2.7

@author: Christopher Di Conza
'''


class GlobalParametersContainer(object):
   
    __amazoncloudusername = ""
    __amazoncloudpassword = ""
    __amazonclouduserkey = ""
    __springforlambdagroupid = ""
    __bucketname = ""
    __bucketregion = ""
    __dblink = ""
    __dbname = ""
    __dbusername = ""
    __dbpassword = ""
    __amazons3filename = ""
    __time = ""
    __sessionid = ""
    __path = ""
    __connectorname = ""
    __connectortype = ""
    __schedulename = ""
    __jsonarray = ""
    __sourceservertype = ""
    __dtstablename = ""
    __orderheadertablename = ""
    __orderaddresstablename = ""
    __orderdetailtablename = ""
    __customername = ""
    __workflowname = ""
    __configid = ""
    __source = ""
    __target = ""
    __sessenderemailaddress = ""
    __sesreceiveremailaddress = list()
    __sessmtpusername = ""
    __sessmtppassword = ""
    __sessmtphost = ""
    __sessmtpport = ""
    
    ##
    ## Accessors and Mutators for global parameters container
    ##
  
    def getAmazoncloudusername(self):
        return self.__amazoncloudusername
    def getAmazoncloudpassword(self):
        return self.__amazoncloudpassword
    def getAmazonclouduserkey(self):
        return self.__amazonclouduserkey
    def getSpringforlambdagroupid(self):
        return self.__springforlambdagroupid
    def getBucketname(self):
        return self.__bucketname
    def getBucketregion(self):
        return self.__bucketregion
    def getDblink(self):
        return self.__dblink
    def getDbname(self):
        return self.__dbname
    def getDbusername(self):
        return self.__dbusername
    def getDbpassword(self):
        return self.__dbpassword
    def getAmazons3filename(self):
        return self.__amazons3filename
    def getTime(self):
        return self.__time
    def getSessionid(self):
        return self.__sessionid
    def Path(self):
        return self.__path
    def getConnectorname(self):
        return self.__connectorname
    def getConnectortype(self):
        return self.__connectortype
    def getSchedulename(self):
        return self.__schedulename
    def getJsonarray(self):
        return self.__jsonarray
    def getSourceservertype(self):
        return self.__sourceservertype
    def getDtstablename(self):
        return self.__dtstablename
    def getOrderheadertablename(self):
        return self.__orderheadertablename
    def getOrderaddresstablename(self):
        return self.__orderaddresstablename
    def getOrderdetailtablename(self):
        return self.__orderdetailtablename
    def getCustomername(self):
        return self.__customername
    def getWorkflowname(self):
        return self.__workflowname
    def getConfigid(self):
        return self.__configid
    def getSource(self):
        return self.__source
    def getTarget(self):
        return self.__target
    def getSessenderemailaddress(self):
        return self.__sessenderemailaddress
    def getSesreceiveremailaddress(self):
        return self.__sesreceiveremailaddress
    def getSessmtpusername(self):
        return self.__sessmtpusername
    def getSessmtppassword(self):
        return self.__sessmtppassword
    def getSessmtphost(self):
        return self.__sessmtphost
    def getSessmtpport(self):
        return self.__sessmtpport
    
    
    
    
    def setAmazoncloudusername(self, value):
        self.__amazoncloudusername = value
    def setAmazoncloudpassword(self, value):
        self.__amazoncloudpassword = value
    def setAmazonclouduserkey(self, value):
        self.__amazonclouduserkey = value
    def setSpringforlambdagroupid(self, value):
        self.__springforlambdagroupid = value
    def setBucketname(self, value):
        self.__bucketname = value
    def setBucketregion(self, value):
        self.__bucketregion = value
    def setDblink(self, value):
        self.__dblink = value
    def setDbname(self, value):
        self.__dbname = value
    def setDbusername(self, value):
        self.__dbusername = value
    def setDbpassword(self, value):
        self.__dbpassword = value
    def setAmazons3filename(self, value):
        self.__amazons3filename = value
    def setTime(self, value):
        self.__time = value
    def setSessionid(self, value):
        self.__sessionid = value
    def setPath(self, value):
        self.__path = value
    def setConnectorname(self, value):
        self.__connectorname = value
    def setConnectortype(self, value):
        self.__connectortype = value
    def setSchedulename(self, value):
        self.__schedulename = value
    def setJsonarray(self, value):
        self.__jsonarray = value
    def setSourceservertype(self, value):
        self.__sourceservertype = value
    def setDtstablename(self, value):
        self.__dtstablename = value
    def setOrderheadertablename(self, value):
        self.__orderheadertablename = value
    def setOrderaddresstablename(self, value):
        self.__orderaddresstablename = value
    def setOrderdetailtablename(self, value):
        self.__orderdetailtablename = value
    def setCustomername(self, value):
        self.__customername = value
    def setWorkflowname(self, value):
        self.__workflowname = value
    def setConfigid(self, value):
        self.__configid = value
    def setSource(self, value):
        self.__source = value
    def setTarget(self, value):
        self.__target = value
    def setSessenderemailaddress(self, value):
        self.__sessenderemailaddress = value
    def setSesreceiveremailaddress(self, value):
        self.__sesreceiveremailaddress = value
    def setSessmtpusername(self, value):
        self.__sessmtpusername = value
    def setSessmtppassword(self, value):
        self.__sessmtppassword = value
    def setSessmtphost(self, value):
        self.__sessmtphost = value
    def setSessmtpport(self, value):
        self.__sessmtpport = value
        

    
    
    
    ##
    ## Constructor for Global Parameters Container
    ##
    def __init__(self):
         
        self.__amazoncloudusername = ""
        self.__amazoncloudpassword = ""
        self.__amazonclouduserkey = ""
        self.__springforlambdagroupid = ""
        self.__bucketname = ""
        self.__bucketregion = ""
        self.__dblink = ""
        self.__dbname = ""
        self.__dbusername = ""
        self.__dbpassword = ""
        self.__amazons3filename = ""
        self.__time = ""
        self.__sessionid = ""
        self.__path = ""
        self.__connectorname = ""
        self.__connectortype = ""
        self.__schedulename = ""
        self.__jsonarray = ""
        self.__sourceservertype = ""
        self.__dtstablename = ""
        self.__orderheadertablename = ""
        self.__orderaddresstablename = ""
        self.__orderdetailtablename = ""
        self.__customername = ""
        self.__workflowname = ""
        self.__configid = ""
        self.__source = ""
        self.__target = ""
        self.__sessenderemailaddress = ""
        self.__sesreceiveremailaddress = list()
        self.__sessmtpusername = ""
        self.__sessmtppassword = ""
        self.__sessmtphost = ""
        self.__sessmtpport = ""



  
            