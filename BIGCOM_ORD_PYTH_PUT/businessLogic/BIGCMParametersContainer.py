'''
Created on Apr 17, 2017
Adaptation of the original java version for Python 2.7

@author: Christopher Di Conza
'''
from businessLogic.GlobalParametersContainer import GlobalParametersContainer
    ##
    ## Bigcommerce subclass extends global parameters container
    ##
class BIGCMParametersContainer(GlobalParametersContainer):

    __bigcmusername= ""   
    __bigcmpassword= "" 
    __bigcmemail= ""    
    __bigcmpage= ""  
    __bigcmstatus= ""
    __bigcmlimit= "" 
    __bigcmapiurl= "" 
    __bigcmlastupdateafter= "" 
    __bigcmlastupdatebefore= "" 
    __bigcmcreatedafter= "" 
    __bigcmcreatedbefore= "" 
    __bigcmpaymentmethod= "" 
    __bigcmminid= ""
    __bigcmmaxid= ""
    __bigcmmintotal= ""
    __bigcmmaxtotal= ""
    __bigcmisdeleted= ""
    
    
    ##
    ## Accessors and Mutators for Shopify subclass
    ##
    
    def getBigcmusername(self):
        return self.__bigcmusername
    def getBigcmpassword(self):
        return self.__bigcmpassword
    def getBigcmemail(self):
        return self.__bigcmemail
    def getBigcmpage(self):
        return self.__bigcmpage
    def getBigcmstatus(self):
        return self.__bigcmstatus
    def getBigcmlimit(self):
        return self.__bigcmlimit
    def getBigcmapiurl(self):
        return self.__bigcmapiurl
    def getBigcmlastupdateafter(self):
        return self.__bigcmlastupdateafter
    def getBigcmlastupdatebefore(self):
        return self.__bigcmlastupdatebefore
    def getBigcmcreatedafter(self):
        return self.__bigcmcreatedafter
    def getBigcmcreatedbefore(self):
        return self.__bigcmcreatedbefore
    def getBigcmpaymentmethod(self):
        return self.__bigcmpaymentmethod
    def getBigcmminid(self):
        return self.__bigcminid
    def getBigcmmaxid(self):
        return self.__bigcmmaxid
    def getBigcmmintotal(self):
        return self.__bigcmmintotal
    def getBigcmmaxtotal(self):
        return self.__bigcmmaxtotal
    def getBigcmisdeleted(self):
        return self.__bigcmisdeleted
    
    
    
    
    def setBigcmusername(self, value):
            self.__bigcmusername = value
    def setBigcmpassword(self, value):
        self.__bigcmpassword = value
    def setBigcmemail(self, value):
        self.__bigcmemail = value
    def setBigcmpage(self, value):
        self.__bigcmpage = value
    def setBigcmstatus(self, value):
        self.__bigcmstatus = value
    def setBigcmlimit(self, value):
        self.__bigcmlimit = value
    def setBigcmapiurl(self, value):
        self.__bigcmapiurl = value
    def setBigcmlastupdateafter(self, value):
        self.__bigcmlastupdateafter = value
    def setBigcmlastupdatebefore(self, value):
        self.__bigcmlastupdatebefore = value
    def setBigcmcreatedafter(self, value):
        self.__bigcmcreatedafter = value
    def setBigcmcreatedbefore(self, value):
        self.__bigcmcreatedbefore = value
    def setBigcmpaymentmethod(self, value):
        self.__bigcmpaymentmethod = value
    def setBigcmminid(self, value):
        self.__bigcmminid = value
    def setBigcmmaxid(self, value):
        self.__bigcmmaxid = value
    def setBigcmmintotal(self, value):
        self.__bigcmmintotal = value
    def setBigcmmaxtotal(self, value):
        self.__bigcmmaxtotal = value
    def setBigcmisdeleted(self, value):
        self.__bigcmisdeleted = value
        
    
    
    ##
    ## Constructor for Bigcommerce
    ##
    def __init__(self):
        self.__bigcmusername= ""   
        self.__bigcmpassword= "" 
        self.__bigcmemail= ""    
        self.__bigcmpage= ""  
        self.__bigcmstatus= ""
        self.__bigcmlimit= "" 
        self.__bigcmapiurl= "" 
        self.__bigcmlastupdateafter= "" 
        self.__bigcmlastupdatebefore= "" 
        self.__bigcmcreatedafter= "" 
        self.__bigcmcreatedbefore= "" 
        self.__bigcmpaymentmethod= "" 
        self.__bigcmminid= ""
        self.__bigcmmaxid= ""
        self.__bigcmmintotal= ""
        self.__bigcmmaxtotal= ""
        self.__bigcmisdeleted= ""
    
    
    
    