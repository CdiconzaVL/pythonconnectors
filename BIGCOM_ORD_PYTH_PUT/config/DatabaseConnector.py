'''
Created on Apr 19, 2017

This class is meant to facilitate database access from other classes.
Based on the Java version from our connector projects. 
@author: Christopher Di Conza
'''
import time
import MySQLdb
import sys
import unicodedata
    
class DatabaseConnector():

    # vlwebservice
#     __dbName = "VLWebService"
#     __driver = "com.mysql.jdbc.Driver" #not used?
#     __dbUsername = "sqladmin"
#     __dbPassword = "VL1omni!$"
#     __dbHostname = "vl-aurora1.cdpcz0fpc2s4.us-east-1.rds.amazonaws.com"
#     __port = "3306"
#     __conn = None
#      currTime = time.strftime("20%y-%m-%d/ %H:%M:%S")
    
    # My local machine
    __dbName = "sys"
    __driver = "com.mysql.jdbc.Driver"  # not used?
    __dbUsername = "root"
    __dbPassword = "chrsqlR1_"
    __dbHostname = "127.0.0.1"
    __port = "3306"
    __conn = None
    currTime = time.strftime("20%y-%m-%d/ %H:%M:%S")
    
    # #
    # #  Constructor for Database Connector with default values passed if the user doesn't supply any
    # #  conn is MySQLdb.connect(hostname, dbUsername, dbPassword, dbName, connect_timeout=5)
    # #
    # #  Example usage:  
    # #  mydbc = DatabaseConnector("VLWebService", "sqladmin",  "Virt2ual!", "vl-aurora1.cdpcz0fpc2s4.us-east-1.rds.amazonaws.com")
    def __init__(self, dbName="VLWebService",
                 dbUsername="sqladmin", dbPassword="Virt2ual!",
                 dbHostname="vl-aurora1.cdpcz0fpc2s4.us-east-1.rds.amazonaws.com",
                conn=None):
         
        self.__dbName = dbName 
        self.__dbUsername = dbUsername
        self.__dbPassword = dbPassword
        self.__dbHostname = dbHostname
        self.__conn = conn
        
    
    # #    
    # # Connect to Database
    # #
    def connectToDatabase(self):
        try:
            
            self.__conn = MySQLdb.connect(self.__dbHostname, self.__dbUsername, self.__dbPassword, self.__dbName, connect_timeout=5)
            
            print("Connection to database established.")
            return 
        except:
            e = sys.exc_info()[0]
            print("***Cannot connect to database. Error:" + str(e)) 
            return
    
        
    # #
    # # Refresh connection - not needed in Python?
    # #
    def refreshConnection(self):
        
        try:
            if self.__conn is not None:
                self.__conn.close()
                self.connectToDatabase()
        except:
            e = sys.exc_info()[0]
            print("***Failed to refresh connection to database. Error:" + str(e))
        return
            
            
            
    # #   
    # # performs general select query from database
    # # takes in table name and additional query if needed (ex. where clause)
    # #
    # # Example usage:
    # # mydbc.generalSelectFromDB("Customer", "WHERE last_name = 'DC'")   additionalQuery is optional.
    
    def generalSelectFromDB(self, tablename, additionalQuery):
        try:
            
            cursor = self.__conn.cursor()
            cursor.execute("SELECT * FROM  %s  %s" % (tablename, additionalQuery))
            data = cursor.fetchall()
            print data
        except:
            e = sys.exc_info()[0]
            print("***Cannot execute Select query. Error:" + str(e)) 
            
            
            
    # # 
    # # performs a general insert statement based on information supplied from the calling class
    # # takes in table name, columns to be inserted into, and the actual values to be entered.
    # # *** please note that when calling this method, your columns and values must be comma separated 
    # # and values must be enclosed in '' quotes inside your string parameter. 
    # #
    # #Insert date as yyyy-MM-dd HH:mm:ss -> strftime('%Y-%m-%d %H:%M:%S')
    # # 
    # #Example usage: 
    # #    table = "Customer"
    # #    columns = "email, first_name, last_name, date_created, date_updated, inserted"
    # #    values = "'cdiconza@virtuallogistics.ca', 'chris', 'diconza', '2017-04-19 11:29:10', '2017-04-19 11:29:10','2017-04-19 11:29:10'"
    # #        
    # #    mydbc.generalInsertToDB(table, columns, values)
        
    def generalInsertToDB(self, tablename, columns, values):
        currTime = time.strftime("%Y-%m-%d %H:%M:%S")
        try:
            cursor = self.__conn.cursor()
            insertStatement = "INSERT INTO %s (%s) VALUES(%s)" % (tablename, columns, values)
            insertStatement = unicodedata.normalize('NFKD', insertStatement).encode('utf8')
            cursor.execute(insertStatement)
            
            self.__conn.commit()
            cursor.close()
            print "Successfully inserted a row. inserted at: " + currTime
        except:
            e = sys.exc_info()[0]
            print("***Cannot execute Insert statement. Error:" + str(e)) 
            
            

       
    # #   
    # # This function is designed for mass inserts so it just takes one long string of several inserts, one after the other.
    # # Execute many is not working at the moment
    # #
        
#     def massInsertToDB(self, insertDef, valuesToInsert):   
#         try:
#             cursor = self.__conn.cursor()
# 
#             data = [("ajoshi@virtuallogistics.ca", "Abhishek", "Joshi", "2017-03-15 10:14:32", "2017-04-18 15:15:15", "2017-04-21 13:31:14"),("ajoshi@virtuallogistics.ca", "Abhishek", "Joshi", "2017-03-15 10:14:32", "2017-04-18 15:15:15", "2017-04-21 13:31:14")]
#             
#            
#             #data.encode('ascii', 'ignore')
#             cursor.executemany(insertDef, valuesToInsert)
#                         
#              
#             self.__conn.commit()
#             cursor.close()
#              currTime = time.strftime("%Y-%m-%d %H:%M:%S")
#             print "Successfully finished mass data Insertion to table at " + currTime
#         except:
#             e = sys.exc_info()[0]
#             print("***Cannot execute mass Insert statement. Error:" + str(e)) 
#          
#      



    # #
    # # general update statement
    # # takes in table name, columns to be updated, and values to be updated and an additionalQuery for the where clause
    # # Note that columns and values need to be a list of equal length
    # #
    # # Example usage:
    # #    colList = ["email", "first_name"]
    # #    valList = ["updatedProgrammatically", "Christopher"]
    # #    additionalQuery = "last_name = 'DC'"
    # #    mydbc.generalUpdateToDB("Customer", colList, valList, additionalQuery)
    # #
    
    def generalUpdateToDB(self, tablename, columns, values, additionalQuery): 
        try:
            currTime = time.strftime("%Y-%m-%d %H:%M:%S")
            colValMap = ""
            for i in range(0, len(columns)):
                if i == len(columns) - 1:
                    colValMap += colValMap + columns[i] + "= '" + values[i] + "' " 
                else:
                    colValMap += colValMap + columns[i] + "= '" + values[i] + "' " + ", "  # code to write comma to all lines except last    
            cursor = self.__conn.cursor()            
            updateStatement = "UPDATE %s SET %s WHERE %s" % (tablename, colValMap, additionalQuery)
            cursor.execute(updateStatement)
            self.__conn.commit()
            cursor.close()
            print "Successfully updated " + currTime
        except:
            e = sys.exc_info()[0]
            print("***Failed to update " + tablename + "! Error:" + str(e))
        
    # #   
    # # general delete statement
    # # provide the tablename and additional conditions
    # # note that the WHERE is included so don't write WHERE in the conditions argument
    # #
    # # Example usage:
    # # mydbc.generalDeleteFromDB("Customer", "email = 'cdiconza@virtuallogistics.ca'")
        
    def generalDeleteFromDB(self, tablename, conditions):   
        try:
            currTime = time.strftime("20%y-%m-%d/ %H:%M:%S")
            cursor = self.__conn.cursor()
            deleteStatement = "DELETE FROM %s WHERE %s" % (tablename, conditions)
            cursor.execute(deleteStatement)
            self.__conn.commit()
            cursor.close()
            print "Successfully deleted " + currTime
        except:
            e = sys.exc_info()[0]
            print("***Failed to delete from " + tablename + "! Error:" + str(e))
            
    
    # #    
    # # return number of rows    
    # #
    # # Example usage:
    # # numRows = mydbc.getRowCount("Customer", " ")
    
    def getRowCount(self, tablename, additionalQuery):    
        try:
            cursor = self.__conn.cursor()
            selectStatement = "SELECT COUNT(*) FROM %s %s" % (tablename, additionalQuery)
            cursor.execute(selectStatement)
            data = cursor.fetchone()
            return data[0]  # [0] needed to return just a number, otherwise it is like "(7L,)"...
        except:
            e = sys.exc_info()[0]
            print("***Failed to get row count " + tablename + "! Error:" + str(e))
        
        
    # #
    # # Call stored procedure
    # # Translated to Python 2.7 but untested as of 2017-04-19
    # #
    def callStoredProcedure(self, name, *args):
        
        str_list = []

        str_list.append(',')
        parametersPlaceholder = ''.join(str_list)
        for arg in args:
            str_list.append('?')
            
        query = "{CALL %s(%s)}" % (name, parametersPlaceholder)
                
        stmt = self.__conn.prepareCall(query);
        
        for arg in args:
            stmt.setString(arg + 1, args[arg]);
        
        
        stmt.executeQuery();
        
    # #
    # # Create a stored procedure
    # # Translated to Python 2.7 but untested as of 2017-04-19
    # #
    def createStoredProcedure(self, name):  
        name = name.upper() 
        self.deleteStoredProcedure(name)
        
        createQuery = "CREATE PROCEDURE `%s` () BEGIN\n\nEND;"
        stmt = self.__conn.prepareCall("" + createQuery + " " + name + "")
        stmt.execute()
        
    # #
    # # Delete a stored procedure
    # # Translated to Python 2.7 but untested as of 2017-04-19
    # #  
    
    def deleteStoredProcedure(self, name):
        name = name.upper()
        deleteQuery = "DROP procedure IF EXISTS `%s`;"
        stmt = self.__conn.prepareCall("" + deleteQuery + " " + name + "")
        stmt.execute() 
        
        
    # #
    # # get a stored procedure's definition
    # # Translated to Python 2.7 but untested as of 2017-04-19
    # #    
    def getStoredProcedureDefinition(self, name):
        name = name.upper()
        query = "SHOW CREATE PROCEDURE " + name
        try:
            statment = self.__conn.prepareStatement(query);
            result = statment.executeQuery();
            result.next();
            return result.getString("Create Procedure");
        except:
            e = sys.exc_info()[0]
            print "Failed to get definition for the stored procedure '" + name + "'", e
        
        
    # #
    # # update a stored procedure
    # # Translated to Python 2.7 but untested as of 2017-04-19
    # # 
        
    def updateStoredProcedure(self, name, procedureDefinition):
        try:
            statment = self.__conn.createStatement();
            statment.addBatch(procedureDefinition);
            statment.executeBatch();
        except:
            e = sys.exc_info()[0]
            if(e.getErrorCode() == 1304):  # this probably won't work
                self.deleteStoredProcedure(name)
                self.updateStoredProcedure(name, procedureDefinition)
            
        
        
        
        
     
     

        
        
        
        
        

    
