'''
Created on May 4, 2017
Adaptation of original Java version for Python 2.7

@author: Christopher Di Conza
'''
from businessLogic.GlobalParametersContainer import GlobalParametersContainer
    ##
    ## LMSTD subclass extends global parameters container
    ##
class LMSTDParametersContainer(GlobalParametersContainer):
    
    __lmstdbearer = ""
    __lmstdurl = ""
    __lmstdstatus = ""    
    
    ##
    ## Accessors and Mutators for LemonStand subclass
    ##

    def getlmstdbearer(self):
        return self.__lmstdbearer
    def getlmstdurl (self):
        return self.__lmstdurl  
    def getlmstdstatus(self):
        return self.__lmstdstatus 
    
        
    def setlmstdbearer(self, value):
        self.__lmstdbearer = value
    def setlmstdurl(self, value):
        self.__lmstdurl = value
    def setlmstdstatus(self, value):
        self.__lmstdstatus = value
        
    ##
    ## Constructor for Shopify Subclass.
    ##
    def __init__(self):
        self.__shpfyusername = ""
        self.__shpfypassword = ""
        self.__shpfypage = ""