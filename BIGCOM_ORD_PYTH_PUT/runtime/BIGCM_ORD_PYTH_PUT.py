'''
Created on Apr 17, 2017
    
    Note that when we merge to Python 3 all bytecode strings will become Unicode (UTF-8) 
    This will resolve many issues, resulting in some try-catch blocks becoming useless 


    Connection Info:
     BigCommerce 
     Big Commerce sandbox info Your URL: https://vlomni10.mybigcommerce.com/admin
     Username: sabeaver@virtuallogistics.ca
     Password: Asdf1234!
     Your Support PIN: 495665
     user for api: api-user 
     api url: https://store-55jq8k5eco.mybigcommerce.com/api/v2/ 
     api token: 7b03185e8da615dd54b1a31de264e91a621a3b5b
     
@author: Christopher Di Conza
'''
import time
import MySQLdb
import sys
import json    
import simplejson
#import bigcommerce
import datetime
import math
import requests
    
from businessLogic.GlobalParametersContainer import GlobalParametersContainer
from businessLogic.BIGCMParametersContainer import BIGCMParametersContainer
from config.DatabaseConnector import DatabaseConnector

def main():

 
    
    
    ##     
    ## performs insert to database
    ## using data from JSON object
    ## to store information on Bigcommerce Orders 
    ##
    def generateInsertOrderIntoDB(JSONObj, tablename):
##WebOrderHeader
        if(tablename == "WOH"):
            try: 
                loadedJSON = json.loads(JSONObj) 
                myOrderDataString = loadedJSON['WOHOrder']
                
                firstHalfOrdered = str(myOrderDataString['OrderDate'])[:-15].strip()
                secondHalfOrdered = str(myOrderDataString['OrderDate'])[11:-6].strip()
                
                #FOR DATES: always insert into "yyyy-MM-dd HH:mm:ss" format  
                dateOrdered = str(firstHalfOrdered + " " +  secondHalfOrdered)
                
                
                table = "weborderheader" #CDC the case may change for prod - WebOrderHeader
                columns = ""
                values = ""
                 
                mydbc.generalInsertToDB(table, columns, values)          
            except:
                e = sys.exc_info()[0]
                print("***Cannot execute SHPFY WEBORDERHEADER Insert statement. Error:" + str(e))  
            
        ##WebOrderDetail
        elif(tablename == "WOD"):
            try: 
                loadedJSON = json.loads(JSONObj) 
                myOrderDataString = loadedJSON['WODOrder']
            
                table = "weborderdetail" #CDC the case may change for prod - WebOrderDetail
                columns = ""
                values = ""
            
                mydbc.generalInsertToDB(table, columns, values)          
            except:
                e = sys.exc_info()[0]
                print("***Cannot execute SHPFY WEBORDERDETAIL Insert statement. Error:" + str(e))  
            
        ##WebOrderAddress
        elif(tablename == "WOA"):
            try: 
                loadedJSON = json.loads(JSONObj) 
                myOrderDataString = loadedJSON['WOAOrder']
                
                table = "weborderaddresses" #CDC the case may change for prod - WebOrderAddresses
                columns = ""
                values = ""
            
            
                mydbc.generalInsertToDB(table, columns, values)          
            except:
                e = sys.exc_info()[0]
                print("***Cannot execute SHPFY WEBORDERADDRESSES Insert statement. Error:" + str(e)) 
        else:
            print("Tablename parameter must be either WOD (web order detail), WOH (web order header), or WOA (web order addresses)") 
    
    ##     
    ## performs insert database
    ## using data from JSON object
    ## to store information on Bigcommerce Products 
    ##
    def generateInsertProductIntoDB(JSONObjProd):        
        try: 
            currTime = time.strftime('%Y-%m-%d %H:%M:%S')           
            #get the product property of the JSON
            loadedJSON = json.loads(JSONObjProd)
            myProductDataString = loadedJSON['product']
                        
            
            #FOR DATES: always insert into "yyyy-MM-dd HH:mm:ss" 
            table1 = "bigcm_products_work"
            columns1 = "name, price, categories, type, availability, weight"
            values1 = "'" + myProductDataString['name'] + "', '" + myProductDataString['price'] + "', '" + myProductDataString['categories'] + "', '" + myProductDataString['type'] + "', '" + myProductDataString['availability'] + "', '" + myProductDataString['weight'] + "'"
            
            #may need multiple statements if different values are needed for different tables.
            mydbc.generalInsertToDB(table1, columns1, values1)
           
        except:
            e = sys.exc_info()[0]
            print("***Cannot execute BIGCM product Insert statement. Error:" + str(e))   
        
    ##     
    ## performs insert database
    ## using data from JSON object
    ## to store information on Bigcommerce customers.
    ##
    def generateInsertCustomerIntoDB(JSONObjCust):
        try:
            currTime = time.strftime('%Y-%m-%d %H:%M:%S')
            #get the customer property of the JSON
            loadedJSON = json.loads(JSONObjCust)
            myCustDataString = loadedJSON['customer'] #copy this to do another layer into address
            
            
            #FOR DATES: always insert into "yyyy-MM-dd HH:mm:ss" format 
            dateCreated = str(myCustDataString['date_created'][4:-5]).strip()
            dateCreatedFormatted = datetime.datetime.strptime(dateCreated, "%d %b %Y %H:%M:%S").strftime("%Y-%m-%d %H:%M:%S")
            
            dateUpdated = str(myCustDataString['date_modified'][4:-5]).strip()
            dateUpdatedFormatted = datetime.datetime.strptime(dateUpdated, '%d %b %Y %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')
                          
            #insertStatement = "INSERT INTO VLWebService.Customer(email, first_name, last_name, date_created, date_updated)" + "VALUES ('" + myCustDataString['email'] + "', '" + myCustDataString['first_name'] + "', '" + myCustDataString['last_name'] + "', '" + dateCreatedFormatted + "', '" + dateUpdatedFormatted +  "')"         
            
            table = "Customer"
            columns = "email, first_name, last_name, date_created, date_updated, inserted"
            values = "'" + myCustDataString['email'] + "', '" + myCustDataString['first_name'] + "', '" + myCustDataString['last_name'] + "', '" + dateCreatedFormatted + "', '" + dateUpdatedFormatted + "', '" + currTime +  "'"
            
            mydbc.generalInsertToDB(table, columns, values)
            
        except:
            e = sys.exc_info()[0]
            print("***Cannot execute BIGCM customer Insert statement. Error:" + str(e))   
    ##
    ## Test function to ensure parameters containers are working.
    ##
    def parametersContainerTestFunction():
        #Testing global parameters and Shopify parameters - getters and setters work as of 2017-04-11
        myParamCont = GlobalParametersContainer()
        myParamCont.setAmazoncloudpassword("testValue")
        print myParamCont.getAmazoncloudpassword()
        
        myParamContBIGCM = BIGCMParametersContainer()
        myParamContBIGCM.setBigcmusername("newUSername")
        print myParamContBIGCM.getBigcmusername()
         
   
        #*** Not needed now that we do a request with the GET statement
#     def connectToBIGCMStore():
#         ##
#         ## Log-In Credentials for Bigcommerce:
#         ##   
#         username = "api-user"
#         apiToken = "7b03185e8da615dd54b1a31de264e91a621a3b5b"
#         hostName = "vlomni10.mybigcommerce.com" 
#         #Private Apps (Basic Auth)
#         shop = bigcommerce.api.BigcommerceApi(host=hostName, basic_auth=(username, apiToken))
#         print "Connected to " + username +  " successfully."
#          
#         return shop
    
    
    #testing code...       
    #parametersContainerTestMethod() 
        
    #Dev DB     
    #mydbc = DatabaseConnector("VLWebService", "sqladmin",  "VL1omni!$!", "vl-aurora1.cdpcz0fpc2s4.us-east-1.rds.amazonaws.com")
    #my local DB
    mydbc = DatabaseConnector("sys", "root",  "chrsqlR1_", "127.0.0.1")
    mydbc.connectToDatabase()
    
#VL WebStore
    getCountCustomerStatement = "https://vlomni10.mybigcommerce.com/api/v2/customers/count.json"
    getCountProductStatement = "https://vlomni10.mybigcommerce.com/api/v2/products/count.json"
    getCountOrderStatement = "https://vlomni10.mybigcommerce.com/api/v2/orders/count.json"
    getCustomersStatement = "https://vlomni10.mybigcommerce.com/api/v2/customers.json?limit=10&page="
    getProductsStatement = "https://vlomni10.mybigcommerce.com/api/v2/products.json?limit=10&page="
    getOrdersStatement = "https://vlomni10.mybigcommerce.com/api/v2/orders.json?limit=10&page="
    ShopUsername = "api-user"
    ShopPassword = "7b03185e8da615dd54b1a31de264e91a621a3b5b" #AKA the API token
    
    currTime = time.strftime('%Y-%m-%d %H:%M:%S')

    #shop = connectToBIGCMStore()
 
    myRequestCust = requests.get(getCountCustomerStatement, auth=(ShopUsername, ShopPassword))
    reqObj1 = json.loads(myRequestCust.content)
    totalNumCustomers =  reqObj1['count']
    current_page = 1
    page_size = 10.0               #250 is max
    custPages = math.ceil(totalNumCustomers / page_size)
    print str(totalNumCustomers) + " customers (" + str(custPages) + " pages)..."
     
    myRequestProd = requests.get(getCountProductStatement, auth=(ShopUsername, ShopPassword))
    reqObj2 = json.loads(myRequestProd.content)
    totalNumProducts =  reqObj2['count']
    prodPages = math.ceil(totalNumProducts / page_size)
    print str(totalNumProducts) + " products (" + str(prodPages) + " pages)..." 
      
    myRequestOrd = requests.get(getCountOrderStatement, auth=(ShopUsername, ShopPassword))
    reqObj3 = json.loads(myRequestOrd.content)
    totalNumOrders = reqObj3['count']
    ordPages = math.ceil(totalNumOrders / page_size)
    print str(totalNumOrders) + " orders (" + str(ordPages) + " pages)..."
 
    #***ORDERS        
    myOrders = []     
    #Must do this while loop to ensure we are not throttled by Shopify's limit
    while(current_page <= ordPages):  #note that current page starts at 1 NOT 0 so therefore <=
        getOrderStatement = getOrdersStatement + str(current_page) + ""
        response = requests.get(getOrderStatement, auth=(ShopUsername, ShopPassword))
        tempJSON = simplejson.loads(str(response.content))
        for order in range(0, len(tempJSON)):
            myOrders.append(tempJSON[order])
        current_page += 1 
        time.sleep(2.05)
     
    print ("generated list of all orders.")
    
    current_page = 1
 
    #***CUSTOMERS
    myCustomers = []    
    #Must do this while loop to ensure we are not throttled by Shopify's limit
    while(current_page <= custPages): #note that current page starts at 1 NOT 0 so therefore <=
        getCustStatement = getCustomersStatement + str(current_page) + ""
        response = requests.get(getCustStatement, auth=(ShopUsername, ShopPassword))
        tempJSON = simplejson.loads(str(response.content))
        for cust in range(0, len(tempJSON)):
            myCustomers.append(tempJSON[cust])
        current_page += 1  
        time.sleep(2.05)
    
    print ("generated list of all customers.")    
    
    current_page = 1 #reset this for next loop
    
    #***PRODUCTS    
    myProducts = []     
    #Must do this while loop to ensure we are not throttled by Shopify's limit
    while(current_page <= prodPages): #note that current page starts at 1 NOT 0 so therefore <=
        getProdStatement = getProductsStatement + str(current_page) + ""
        response = requests.get(getProdStatement, auth=(ShopUsername, ShopPassword))
        tempJSON = simplejson.loads(str(response.content))
        for prod in range(0, len(tempJSON)):
            myProducts.append(tempJSON[prod])
        current_page += 1 
        time.sleep(2.05)
    
    print ("generated list of all products.")
        
  
    #***ORDER    
    for o in range(0, len(myOrders)):
        counter = 0 # reset counter every order.
        ordData = "{ \"order\": { \"id\":\"" +  str(myOrders[o]['id']) + "\"} }"
        print ordData 
        generateInsertOrderIntoDB(ordData, "tablename")
    

    #***CUSTOMER
    for c in range(0, len(myCustomers)):
        
        try:
            custFirstName = myCustomers[c]['first_name']
            custFirstName = custFirstName.encode('utf8')
        except:
            custFirstName = "None"
        try:
            custLastName = myCustomers[c]['last_name']
            custLastName = custLastName.encode('utf8')
        except:
            custLastName = "None"
        try:
            custEmail = myCustomers[c]['email']
            custEmail = custEmail.encode('utf8')
        except:
            custEmail = "None"
                
                
        custData = ("{ \"customer\": { \"email\":\""    +  custEmail 
        + "\",    \"first_name\":\""                    +  custFirstName
        + "\" ,   \"last_name\":\""                     +  custLastName 
        + "\",    \"date_created\":\""                  +  myCustomers[c]['date_created'] 
        + "\",    \"date_modified\":\""                 +  myCustomers[c]['date_modified'] 
        + "\"} }")
        print custData
        generateInsertCustomerIntoDB(custData)
    
    
    #***PRODUCT
    for p in range(0, len(myProducts)):
        #note that categories is a list of integers nested within products.
        
        try:
            prodName = myProducts[p]['name']
            prodName = prodName.encode('utf8')
        except:
            prodName = "None"
            
                   
        categoryString = " ["   
        categoryList = myProducts[p]['categories']
        for myCat in range(0, len(categoryList)):   
            if myCat == len(categoryList) -1:     
                categoryString = categoryString  +  str(categoryList[myCat]) + "]"
            else:
                categoryString = categoryString  +  str(categoryList[myCat]) + " , "
             
        
        #there are over 30 properties so I am just doing the mandatory fields until I have requirements on exactly what is needed.
        productData = ("{ \"product\": { \"name\":\""   +  prodName 
        + "\",    \"price\":\""                         +  myProducts[p]['price'] 
        + "\",    \"categories\":\""                    +  categoryString 
        + "\",    \"type\":\""                          +  myProducts[p]['type'] 
        + "\",    \"availability\":\""                  +  myProducts[p]['availability'] 
        + "\",    \"weight\":\""                        +  myProducts[p]['weight'] 
        + "\"} }")
        print productData
        generateInsertProductIntoDB(productData)
        
        
   
    
if __name__ == '__main__': main()




