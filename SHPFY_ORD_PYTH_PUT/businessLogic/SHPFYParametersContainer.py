'''
Created on Apr 11, 2017
Adaptation of the original java version for Python 2.7

@author: Christopher Di Conza
'''
from businessLogic.GlobalParametersContainer import GlobalParametersContainer
    ##
    ## Shopify subclass extends global parameters container
    ##
class SHPFYParametersContainer(GlobalParametersContainer):

    __shpfyusername = "" 
    __shpfypassword = ""
    __shpfypage = "" 
    __shpfytype = ""
    __shpfyfulfillmentstatus = ""
    __shpfylimit = "" 
    __shpfyupdatetimestart = ""
    __shpfyupdatetimeend = ""
    __shpfynumoffiles = ""
    __shpfyapiurl = ""
    __shpfylastupdateafter = ""
    __shpfylastupdatebefore = ""
    __shpfycreatedafter = ""
    __shpfycreatedbefore = ""
    __shpfyorderstatus = list()   

    ##
    ## Accessors and Mutators for Shopify subclass
    ##
    def getShpfyusername(self):
        return self.__shpfyusername
    def getShpfypassword (self):
        return self.__shpfypassword  
    def getShpfypage(self):
        return self.__shpfypage   
    def getShpfytype(self):
        return self.__shpfytype  
    def getShpfyfulfillmentstatus(self):
        return self.__shpfyfulfillmentstatus    
    def getShpfylimit(self):
        return self.__shpfylimit  
    def getShpfyupdatetimestart(self):
        return self.__shpfyupdatetimestart    
    def getShpfyupdatetimeend(self):
        return self.__shpfyupdatetimeend    
    def getShpfynumoffiles(self):
        return self.__shpfynumoffiles       
    def getShpfyapiurl(self):
        return self.__shpfyapiurl    
    def getShpfylastupdateafter(self):
        return self.__shpfylastupdateafter     
    def getShpfylastupdatebefore(self):
        return self.__shpfylastupdatebefore    
    def getShpfycreatedafter(self):
        return self.__shpfycreatedafter   
    def getShpfycreatedbefore(self):
        return self.__shpfycreatedbefore  
    #Note that this just returns the entire list.  
    def getShpfyorderstatus(self):
        return self.__shpfyorderstatus        
    
    
    
    def setShpfyusername(self, value):
        self.__shpfyusername = value
    def setShpfypassword(self, value):
        self.__shpfypassword = value
    def setShpfypage(self, value):
        self.__shpfypage = value
    def setShpfytype(self, value):
        self.__shpfytype = value
    def setshpfyfulfillmentstatus(self, value):
        self.__shpfyfulfillmentstatus = value
    def setshpfylimit(self, value):
        self.__shpfylimit = value
    def setshpfyupdatetimestart(self, value):
        self.__shpfyupdatetimestart = value
    def setshpfyupdatetimeend(self, value):
        self.__shpfyupdatetimeend = value
    def setshpfynumoffiles(self, value):
        self.__shpfynumoffiles= value
    def shpfyapiurlset(self, value):
        self.__shpfyapiurl = value
    def setshpfylastupdateafter(self, value):
        self.__shpfylastupdateafter = value
    def setshpfylastupdatebefore(self, value):
        self.__shpfylastupdatebefore = value
    def setshpfycreatedafter(self, value):
        self.__shpfycreatedafter = value
    def setshpfycreatedbefore(self, value):
        self.__shpfycreatedbefore = value
    def setshpfyorderstatus(self, value):
        self.__shpfyorderstatus = value
    
    
    
    ##
    ## Constructor for Shopify Subclass.
    ##
    def __init__(self):
        self.__shpfyusername = ""
        self.__shpfypassword = ""
        self.__shpfypage = ""
        self.__shpfytype = ""
        self.__shpfyfulfillmentstatus = ""
        self.__shpfylimit = ""
        self.__shpfyupdatetimestart = ""
        self.__shpfyupdatetimeend = ""
        self.__shpfynumoffiles = ""
        self.__shpfyapiurl = ""
        self.__shpfylastupdateafter = ""
        self.__shpfylastupdatebefore = ""
        self.__shpfycreatedafter = ""
        self.__shpfycreatedbefore = ""
        self.__shpfyorderstatus = ""