# -*- coding: utf-8 -*-
'''
Created on May 05, 2017

Note that when we merge to Python 3 all bytecode strings will become Unicode (UTF-8) 
This will resolve many issues, resulting in some try-catch blocks becoming useless 

@author: Christopher Di Conza'''

import requests
import math
import unicodedata
import shopify
import time
import MySQLdb
import sys
import json
import datetime
from timeit import Timer
import simplejson

from businessLogic.GlobalParametersContainer import GlobalParametersContainer
from config.DatabaseConnector import DatabaseConnector


def main():
    
    
     # #     
    # # performs insert database
    # # using data from JSON object
    # # to store information on SHPFY orders
    # #
    def generateInsertOrderIntoDB(JSONObj, tablename):
        currTime = time.strftime('%Y-%m-%d %H:%M:%S')
       
        # #WebOrderHeader
        if(tablename == "WOH"):
            try: 
                loadedJSON = json.loads(JSONObj) 
                myOrderDataString = loadedJSON['WOHOrder']
                
                table = "weborderheader"  # CDC the case may change for prod - WebOrderHeader
                columns = ""
                values = "'"  + "'"
                 
                mydbc.generalInsertToDB(table, columns, values)          
            except:
                e = sys.exc_info()[0]
                print("***Cannot execute LEMONSTAND WEBORDERHEADER Insert statement. Error:" + str(e))  
            
        # #WebOrderDetail
        elif(tablename == "WOD"):
            try: 
                loadedJSON = json.loads(JSONObj) 
                myOrderDataString = loadedJSON['WODOrder']
            
                table = "weborderdetail"  # CDC the case may change for prod - WebOrderDetail
                columns = ""
                values = "'" +  "'"
            
                mydbc.generalInsertToDB(table, columns, values)          
            except:
                e = sys.exc_info()[0]
                print("***Cannot execute LEMONSTAND WEBORDERDETAIL Insert statement. Error:" + str(e))  
            
        # #WebOrderAddress
        elif(tablename == "WOA"):
            try: 
                loadedJSON = json.loads(JSONObj) 
                myOrderDataString = loadedJSON['WOAOrder']
                
                table = "weborderaddresses"  # CDC the case may change for prod - WebOrderAddresses
                columns = ""
                values = "'" + "'"
            
            
                mydbc.generalInsertToDB(table, columns, values)          
            except:
                e = sys.exc_info()[0]
                print("***Cannot execute LEMONSTAND WEBORDERADDRESSES Insert statement. Error:" + str(e)) 
        else:
            print("Tablename parameter must be either WOD (web order detail), WOH (web order header), or WOA (web order addresses)") 
    # #     
    # # performs insert database
    # # using data from JSON object
    # # to store information on LEMONSTAND Products and Variants.
    # #
    # # Note that each variant is a new record
    # #
    def generateInsertProductIntoDB(JSONObjProd):
        currTime = time.strftime('%Y-%m-%d %H:%M:%S')
        try:            
            # get the product property of the JSON
            loadedJSON = json.loads(JSONObjProd)
            myProductDataString = loadedJSON['product']

            #see if there is a variant equivalent and if you need to insert one row per variant!    
            table1 = "lmstd_products_work"
            columns1 = "name, sku, base_price, url_name, id, description"
            values1 = "'" + myProductDataString['name'] + "', '" + myProductDataString['sku'] + "', '" + myProductDataString['base_price'] + "', '" + myProductDataString['url_name'] + "', '" + myProductDataString['id'] + "', '" + myProductDataString['description'] + "'" 
            
            # may need multiple statements if different values are needed for different tables.
            mydbc.generalInsertToDB(table1, columns1, values1)
            
        except:
            e = sys.exc_info()[0]
            print("***Cannot execute LEMONSTAND product Insert statement. Error:" + str(e))  

    # #     
    # # performs insert database
    # # using data from JSON object
    # # to store information on LEMONSTAND customers.
    # #
    def generateInsertCustomerIntoDB(JSONObjCust):
        
        currTime = time.strftime('%Y-%m-%d %H:%M:%S')
        try:
            # get the customer property of the JSON
            loadedJSON = json.loads(JSONObjCust)
            myCustDataString = loadedJSON['customer']
            
            # data comes in as 2017-04-18T15:15:15-0400
            firstHalfCreated = str(myCustDataString['created_at'][:-14]).strip()
            secondHalfCreated = str(myCustDataString['created_at'][11:-5]).strip()
            
            firstHalfUpdated = str(myCustDataString['updated_at'][:-14]).strip()
            secondHalfUpdated = str(myCustDataString['updated_at'][11:-5]).strip()
            
            # FOR DATES: always insert into "yyyy-MM-dd HH:mm:ss" format  
            dateCreated = str(firstHalfCreated + " " + secondHalfCreated)
            
            dateUpdated = str(firstHalfUpdated + " " + secondHalfUpdated)
                        
            table = "Customer"
            columns = "first_name, last_name, email, date_created, date_updated, inserted"
            values = "'" + myCustDataString['first_name'] + "', '" + myCustDataString['last_name'] + "', '"  + myCustDataString['email'] + "', '" + dateCreated + "', '"+ dateUpdated + "', '" + currTime +  "'"
            
            mydbc.generalInsertToDB(table, columns, values)
            
#             outputStr = values
#             return outputStr
                    
        except:
            e = sys.exc_info()[0]
            print("***Cannot execute LEMONSTAND customer Insert statement. Error:" + str(e))    
        
    
    # #
    # # Function to insert notes to a specific order
    # # Pass in your note, your order number
    # #
    def insertNoteToOrder(noteStr, orderNum):
        
        order = shopify.Order()
        orderObj = order.find(orderNum)         
        orderObj.note = noteStr
        #result = orderObj.save()  CDC - uncomment this    *** 
        
        
        
     
    #***PROGRAM START.    

    
        #https://vlomni.lemonstand.com/api/v2/products  
        #headers ->key=  authorization value = Bearer 95F3BjbZxvRejkNysH5w8uqzVRbEFJkiHZPwCj1u
        #This vendor is different - no count, instead there is meta data attached with "total_count"
    # limit hardcoded to 250 - concatenate page_size variable in if needed...
# #VL WebStore
    getCountCustomerStatement = "https://vlomni.lemonstand.com/api/v2/customers"
    getCountProductStatement = "https://vlomni.lemonstand.com/api/v2/products"
    getCountOrderStatement = "https://vlomni.lemonstand.com/api/v2/orders"
    getCustomersStatement = "https://vlomni.lemonstand.com/api/v2/customers?limit=9&page="
    getProductsStatement = "https://vlomni.lemonstand.com/api/v2/products?limit=9&page="
    getOrdersStatement = "https://vlomni.lemonstand.com/api/v2/orders?limit=9&page="
    ShopPassword = "95F3BjbZxvRejkNysH5w8uqzVRbEFJkiHZPwCj1u" #AKA the API token
    # Dev DB     
    # mydbc = DatabaseConnector("VLWebService", "sqladmin",  "VL1omni!$!", "vl-aurora1.cdpcz0fpc2s4.us-east-1.rds.amazonaws.com")
    # my local DB
    mydbc = DatabaseConnector("sys", "root", "chrsqlR1_", "127.0.0.1")
    mydbc.connectToDatabase()
    
    
    
    myRequestCust = requests.get(getCountCustomerStatement, headers={'Authorization': ShopPassword})
    reqObj1 = json.loads(myRequestCust.content)
    totalNumCustomers = reqObj1['meta']['total_count']
    current_page = 1
    page_size = 9.0  # 250 is max
    custPages = math.ceil(totalNumCustomers / page_size)
    print str(totalNumCustomers) + " customers (" + str(custPages) + " pages)..."
     
    myRequestProd = requests.get(getCountProductStatement, headers={'Authorization': ShopPassword})
    reqObj2 = json.loads(myRequestProd.content)
    totalNumProducts = reqObj2['meta']['total_count']
    prodPages = math.ceil(totalNumProducts / page_size)
    print str(totalNumProducts) + " products (" + str(prodPages) + " pages)..." 
      
    myRequestOrd = requests.get(getCountOrderStatement, headers={'Authorization': ShopPassword})
    reqObj3 = json.loads(myRequestOrd.content)
    totalNumOrders = reqObj3['meta']['total_count']
    ordPages = math.ceil(totalNumOrders / page_size)
    print str(totalNumOrders) + " orders (" + str(ordPages) + " pages)..."
    
    
    
    currTime = time.strftime('%Y-%m-%d %H:%M:%S')

    #shop = connectToBIGCMStore()
 
    #***ORDERS        
    myOrders = []     
    #Must do this while loop to ensure we are not throttled by Shopify's limit
    while(current_page <= ordPages): #note that current page starts at 1 NOT 0 so therefore <=
        getOrderStatement = getOrdersStatement + str(current_page) + ""
        response = requests.get(getOrderStatement, headers={'Authorization': ShopPassword})
        tempJSON = simplejson.loads(str(response.content))
        for order in range(0, len(tempJSON['data'])):
            myOrders.append(tempJSON['data'][order])
        current_page += 1 
        time.sleep(1.05)
     
    print ("generated list of all orders.")
    
    current_page = 1
 
    #***CUSTOMERS
    myCustomers = []    
    #Must do this while loop to ensure we are not throttled by Shopify's limit
    while(current_page <= custPages): #note that current page starts at 1 NOT 0 so therefore <=
        getCustStatement = getCustomersStatement + str(current_page) + ""
        response = requests.get(getCustStatement, headers={'Authorization': ShopPassword})
        tempJSON = simplejson.loads(str(response.content))
        for cust in range(0, len(tempJSON['data'])):
            myCustomers.append(tempJSON['data'][cust])
        current_page += 1  
        time.sleep(1.05)
    
    print ("generated list of all customers.")    
    
    current_page = 1 #reset this for next loop
    
    #***PRODUCTS    
    myProducts = []     
    #Must do this while loop to ensure we are not throttled by Shopify's limit
    while(current_page <= prodPages): #note that current page starts at 1 NOT 0 so therefore <=
        getProdStatement = getProductsStatement + str(current_page) + ""
        response = requests.get(getProdStatement, headers={'Authorization': ShopPassword})
        tempJSON = simplejson.loads(str(response.content))
        for prod in range(0, len(tempJSON['data'])):
            myProducts.append(tempJSON['data'][prod])
        current_page += 1 
        time.sleep(1.05)
    
    print ("generated list of all products.")
  
#      #***ORDER    
#     for o in range(0, len(myOrders)):
#         counter = 0 # reset counter every order.
#         ordData = "{ \"order\": { \"id\":\"" +  str(myOrders[o]['id']) + "\"} }"
#         print ordData 
#         generateInsertOrderIntoDB(ordData, "tablename")
    

    #***CUSTOMER
    for c in range(0, len(myCustomers)):
        try:
            myPassword = myCustomers[c]['password']
        except:
            myPassword = "None"   
             
        try:
            custFirstName = myCustomers[c]['first_name']
            custFirstName = custFirstName.encode('utf8')
        except:
            custFirstName = "None"
        try:
            custLastName = myCustomers[c]['last_name']
            custLastName = custLastName.encode('utf8')
        except:
            custLastName = "None"
        try:
            custEmail = myCustomers[c]['email']
            custEmail = custEmail.encode('utf8')
        except:
            custEmail = "None"    
              
        custData = ("{ \"customer\": { \"first_name\":\""   +  custFirstName  
        + "\",    \"last_name\":\""                         +  custLastName
        + "\",    \"password\":\""                          +  myPassword
        + "\",    \"email\":\""                             +  custEmail
        + "\",    \"id\":\""                                +  str(myCustomers[c]['id'])
        + "\",    \"created_at\":\""                        +  myCustomers[c]['created_at'] 
        + "\",    \"updated_at\":\""                        +  myCustomers[c]['updated_at'] 
        + "\"} }")
        print custData
        generateInsertCustomerIntoDB(custData)
    
    
    #***PRODUCT
    #Only extracting the mandatory fields until I have specific requirements on what is needed
    for p in range(0, len(myProducts)): 
        
        try:
            prodName = myProducts[p]['name']
            prodName = prodName.encode('utf8')
        except:
            prodName = "None"
                 
        try:
            prodURLName = myProducts[p]['url_name']
            prodURLName = prodURLName.encode('utf8')
        except:
            prodURLName = "None"
            
        try:
            prodDesc = myProducts[p]['description']
            prodDesc = prodDesc.encode('utf8')
            prodDesc = prodDesc.replace('<p>', '').replace('"', "").replace("'", '').replace('</p>', '').replace('\n', '').replace('\r', '').replace('\u', '').replace('\t', '').replace('\b', '').replace('&bull', '').replace('-', ' ')
        except:
            prodDesc = "None"
        productData = ("{ \"product\": { \"name\":\""   +  prodName
        + "\",    \"sku\":\""                           +  myProducts[p]['sku']
        + "\",    \"base_price\":\""                    +  str(myProducts[p]['base_price'])
        + "\",    \"url_name\":\""                      +  prodURLName
        + "\",    \"id\":\""                            +  str(myProducts[p]['id'])
        + "\",    \"description\":\""                   +  prodDesc
        + "\"} }")
        print productData
        generateInsertProductIntoDB(productData)
    
      
if __name__ == '__main__': main()








