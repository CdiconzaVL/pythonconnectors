'''
Created on Apr 20, 2017

class to test if s3Utils are working as expected given hardcoded data

@author: Christopher Di Conza
'''
from _io import BufferedReader
from s3Utils.ReadFileFromS3 import ReadFileFromS3
from s3Utils.S3upload import S3Upload
from businessLogic.GlobalParametersContainer import GlobalParametersContainer
import untangle
import xml.etree.ElementTree as ET
    
def main():
    bucketname = "middlewareworkflow"
    workflowname = "ShopifyBlueLinkpostOrder"
    customername = "DANESON"
    sessionId = "428071"
    source = "Shopify11order"
    filename = "Aws_S3_ShopifyOrder.xml"
    uploadContent = ""
    GPC = GlobalParametersContainer()
    
    
    rffs3 = ReadFileFromS3()
    path = bucketname + "/" + workflowname + "/" + customername + "/" + sessionId + "/" + source
    inputstream = None
    s3u = S3Upload()
    
    #doc = untangle.parse("TestUploadFromPython.xml")
    doc = ET.parse("TestUploadFromPython.xml")
    boolWorked = s3u.uploadxmlfile("TestUploadFromPython.xml", uploadContent, doc, GPC)
    print boolWorked
    inputstream = rffs3.readfile(path, filename)
    displayTextInputStream(inputstream)


    ##        
    ## Displays the contents of the specified input stream as text.
    ## Takes a stream as the argument
    ##
    def displayTextInputStream(self, myInput): 
        
        br = BufferedReader(myInput)        
        content = br.read()
        print(content)
        
if __name__ == '__main__': main()