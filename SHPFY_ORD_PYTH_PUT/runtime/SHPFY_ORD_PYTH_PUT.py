# -*- coding: utf-8 -*-
'''
Created on Apr 11, 2017

Note that when we merge to Python 3 all bytecode strings will become Unicode (UTF-8) 
This will resolve many issues, resulting in some try-catch blocks becoming useless 

@author: Christopher Di Conza'''

import requests
import math
import unicodedata
import shopify
import time
import MySQLdb
import sys
import json
import datetime
from timeit import Timer
import simplejson

from businessLogic.GlobalParametersContainer import GlobalParametersContainer
from businessLogic.SHPFYParametersContainer  import SHPFYParametersContainer
from config.DatabaseConnector import DatabaseConnector

def main():

    
    # #     
    # # performs insert database
    # # using data from JSON object
    # # to store information on SHPFY orders
    # #
    def generateInsertOrderIntoDB(JSONObj, tablename):
        currTime = time.strftime('%Y-%m-%d %H:%M:%S')
       
        # #WebOrderHeader
        if(tablename == "WOH"):
            try: 
                loadedJSON = json.loads(JSONObj) 
                myOrderDataString = loadedJSON['WOHOrder']
                
                firstHalfOrdered = str(myOrderDataString['OrderDate'])[:-15].strip()
                secondHalfOrdered = str(myOrderDataString['OrderDate'])[11:-6].strip()
                
                # FOR DATES: always insert into "yyyy-MM-dd HH:mm:ss" format  
                dateOrdered = str(firstHalfOrdered + " " + secondHalfOrdered)
                
                
                # NEED TO ADD -> Credit_card_type, Credit_card_name, Credit_card_number, Credit_card_expiry, Credit_card_code, 
                table = "weborderheader"  # CDC the case may change for prod - WebOrderHeader
                columns = "WebOrderNumber, WebOrderID, OrderDate, CreatedDate, Fulfilment_Status, FirstName, LastName, Webstore, Currency, ShipTo, ShipMethod, ShipMethodDesc, ShippingCost, IPAddress, OrderNotes, Subtotal, Discount, Tax, GrandTotal, PercentageDiscount, PaymentMethod"
                values = "'" + myOrderDataString['WebOrderNumber'] + "', '" + myOrderDataString['WebOrderID'] + "', '" + dateOrdered + "', '" + myOrderDataString['CreatedDate'] + "', '" + myOrderDataString['Fulfillment_Status'] + "', '" + myOrderDataString['FirstName'] + "', '" + myOrderDataString['LastName'] + "', '" + myOrderDataString['Webstore'] + "', '" + myOrderDataString['Currency'] + "', '" + myOrderDataString['ShipTo'] + "', '" + myOrderDataString['ShipMethod'] + "', '" + myOrderDataString['ShipMethodDesc'] + "', '" + myOrderDataString['ShippingCost'] + "', '" + myOrderDataString['IPAddress'] + "', '" + myOrderDataString['OrderNotes'] + "', '" + myOrderDataString['Subtotal'] + "', '" + myOrderDataString['Discount'] + "', '" + myOrderDataString['Tax'] + "', '" + myOrderDataString['GrandTotal'] + "', '" + myOrderDataString['PercentageDiscount'] + "', '" + myOrderDataString['PaymentMethod'] + "'"
                 
                mydbc.generalInsertToDB(table, columns, values)          
            except:
                e = sys.exc_info()[0]
                print("***Cannot execute SHPFY WEBORDERHEADER Insert statement. Error:" + str(e))  
            
        # #WebOrderDetail
        elif(tablename == "WOD"):
            try: 
                loadedJSON = json.loads(JSONObj) 
                myOrderDataString = loadedJSON['WODOrder']
            
                table = "weborderdetail"  # CDC the case may change for prod - WebOrderDetail
                columns = "POKEY, POLine, Customer_PO_Line, Sequence, OrderQty, ShipQty, UOM, UnitPrice, SKU, CustomerSKU, UPC, Description, ItemNotes, Tax, Tax2"
                values = "'" + myOrderDataString['POKEY'] + "', '" + myOrderDataString['POLine'] + "', '" + myOrderDataString['Customer_PO_Line'] + "', '" + myOrderDataString['Sequence'] + "', '" + myOrderDataString['OrderQty'] + "', '" + myOrderDataString['ShipQty'] + "', '" + myOrderDataString['UOM'] + "', '" + myOrderDataString['UnitPrice'] + "', '" + myOrderDataString['SKU'] + "', '" + myOrderDataString['CustomerSKU'] + "', '" + myOrderDataString['UPC'] + "', '" + myOrderDataString['Description'] + "', '" + myOrderDataString['ItemNotes'] + "', '" + myOrderDataString['Tax'] + "', '" + myOrderDataString['Tax2'] + "'"
            
                mydbc.generalInsertToDB(table, columns, values)          
            except:
                e = sys.exc_info()[0]
                print("***Cannot execute SHPFY WEBORDERDETAIL Insert statement. Error:" + str(e))  
            
        # #WebOrderAddress
        elif(tablename == "WOA"):
            try: 
                loadedJSON = json.loads(JSONObj) 
                myOrderDataString = loadedJSON['WOAOrder']
                
                table = "weborderaddresses"  # CDC the case may change for prod - WebOrderAddresses
                columns = "AddressCode, AddressType, Name, Address1, Address2, City, Province, PostalCode, Country, Phone, AcctCode, Company"
                values = "'" + myOrderDataString['AddressCode'] + "', '" + myOrderDataString['AddressType'] + "', '" + myOrderDataString['Name'] + "', '" + myOrderDataString['Address1'] + "', '" + myOrderDataString['Address2'] + "', '" + myOrderDataString['City'] + "', '" + myOrderDataString['Province'] + "', '" + myOrderDataString['PostalCode'] + "', '" + myOrderDataString['Country'] + "', '" + myOrderDataString['Phone'] + "', '" + myOrderDataString['AcctCode'] + "', '" + myOrderDataString['Company'] + "'"
            
            
                mydbc.generalInsertToDB(table, columns, values)          
            except:
                e = sys.exc_info()[0]
                print("***Cannot execute SHPFY WEBORDERADDRESSES Insert statement. Error:" + str(e)) 
        else:
            print("Tablename parameter must be either WOD (web order detail), WOH (web order header), or WOA (web order addresses)") 
    # #     
    # # performs insert database
    # # using data from JSON object
    # # to store information on SHPFY Products and Variants.
    # #
    # # Note that each variant is a new record
    # #
    def generateInsertProductIntoDB(JSONObjProd):
        currTime = time.strftime('%Y-%m-%d %H:%M:%S')
        try:            
            # get the product property of the JSON
            loadedJSON = json.loads(JSONObjProd)
            myProductDataString = loadedJSON['product']
            myVariants = myProductDataString['variants']
#             myImages = myProductDataString['images']
#             myOptions = myProductDataString['options']

#             for img in range(0, len(myImages)):
#                 print myImages[img]['id']
#                 
#             for opt in range(0, len(myOptions)):
#                 print myOptions[opt]['id']

            # access any Product property:
#             print myProductDataString['id']
             
            #***CREATED AND UPDATED AT ARE VARIANT PROPERTIES NOT PRODUCT 
             
# new record for each variant
            for var in range(0, len(myVariants)):
                
#                            #data comes in as 2017-04-18T15:15:15-04:00
                firstHalfCreated = str(myVariants[var]['created_at'][:-15]).strip()
                secondHalfCreated = str(myVariants[var]['created_at'][11:-6]).strip()
                
                firstHalfUpdated = str(myVariants[var]['updated_at'][:-15]).strip()
                secondHalfUpdated = str(myVariants[var]['updated_at'][11:-6]).strip()
                
                # FOR DATES: always insert into "yyyy-MM-dd HH:mm:ss" format  
                dateCreated = str(firstHalfCreated + " " + secondHalfCreated)
                
                dateUpdated = str(firstHalfUpdated + " " + secondHalfUpdated) 

                table1 = "shopify_products_work"
                columns1 = "description, name, product_id, product_type, barcode, compare_at_price, created_at, fulfillment_service, grams, variant_id, inventory_management, inventory_policy, inventory_quantity, old_inventory_quantity, option1, option2, option3, position, price, requires_shipping, sku, taxable, title, updated_at, weight, weight_unit "
                values1 = "'" + myProductDataString['body_html'] + "', '" + myProductDataString['handle'] + "', '" + str(myProductDataString['id']) + "', '" + str(myProductDataString['product_type']) + "', '" + str(myVariants[var]['barcode']) + "', '" + str(myVariants[var]['compare_at_price']) + "', '" + dateCreated + "', '" + str(myVariants[var]['fulfillment_service']) + "', '" + str(myVariants[var]['grams']) + "', '" + str(myVariants[var]['id']) + "', '" + str(myVariants[var]['inventory_management']) + "', '" + str(myVariants[var]['inventory_policy']) + "', '" + str(myVariants[var]['inventory_quantity']) + "', '" + str(myVariants[var]['old_inventory_quantity']) + "', '" + str(myVariants[var]['option1']) + "', '" + str(myVariants[var]['option2']) + "', '" + str(myVariants[var]['option3']) + "', '" + str(myVariants[var]['position']) + "', '" + str(myVariants[var]['price']) + "', '" + str(myVariants[var]['requires_shipping']) + "', '" + str(myVariants[var]['sku']) + "', '" + str(myVariants[var]['taxable']) + "', '" + str(myVariants[var]['title']) + "', '" + dateUpdated + "', '" + str(myVariants[var]['weight']) + "', '" + str(myVariants[var]['weight_unit']) + "'" 
                
                # may need multiple statements if different values are needed for different tables.
                mydbc.generalInsertToDB(table1, columns1, values1)
            
        except:
            e = sys.exc_info()[0]
            print("***Cannot execute SHPFY product Insert statement. Error:" + str(e))  

    # #     
    # # performs insert database
    # # using data from JSON object
    # # to store information on SHPFY customers.
    # #
    def generateInsertCustomerIntoDB(JSONObjCust):
        
        currTime = time.strftime('%Y-%m-%d %H:%M:%S')
        try:
            # get the customer property of the JSON
            loadedJSON = json.loads(JSONObjCust)
            myCustDataString = loadedJSON['customer']
            myAddressString = myCustDataString['default_address']
            # access any Product property:
            # print myCustDataString['id']
        
            # data comes in as 2017-04-18T15:15:15-04:00
            firstHalfCreated = str(myCustDataString['created_at'][:-15]).strip()
            secondHalfCreated = str(myCustDataString['created_at'][11:-6]).strip()
            
            firstHalfUpdated = str(myCustDataString['updated_at'][:-15]).strip()
            secondHalfUpdated = str(myCustDataString['updated_at'][11:-6]).strip()
            
            # FOR DATES: always insert into "yyyy-MM-dd HH:mm:ss" format  
            dateCreated = str(firstHalfCreated + " " + secondHalfCreated)
            
            dateUpdated = str(firstHalfUpdated + " " + secondHalfUpdated)
            
            table = "Customer"
            columns = "email, first_name, last_name, date_created, date_updated, inserted"
            values = "'" + myCustDataString['email'] + "', '" + myCustDataString['first_name'] + "', '" + myCustDataString['last_name'] + "', '" + dateCreated + "', '" + dateUpdated + "', '" + currTime + "'"
            
            mydbc.generalInsertToDB(table, columns, values)
            
#             outputStr = values
#             return outputStr
                    
        except:
            e = sys.exc_info()[0]
            print("***Cannot execute SHPFY customer Insert statement. Error:" + str(e))    
        
    # #
    # # Sets up the connection.
    # # return value is the current Shopify store 
    # #
    def connectToSHPFYStore():
#         API_KEY = 'e9530acd78fa8c4c527ccc092e5638e7'
#         PASSWORD = 'b3ae8cb4afc28c16678dfda7581f893e'
#         SHOP_NAME = 'vl-test-store'
#                      
#         shop_url = "https://%s:%s@%s.myshopify.com/admin" % (API_KEY, PASSWORD, SHOP_NAME)
#         shopify.ShopifyResource.set_site(shop_url)
#         shop = shopify.Shop.current()
                 
       # Unique Salon Concepts - for testing purposes...
        API_KEY = '073f51bd8cc118dea2003af3d785466a'
        PASSWORD = 'e906a4d2e38741bb279d3f152c9f0f99'
        SHOP_NAME = 'unique-salon-concepts'
                   
        shop_url = "https://%s:%s@%s.myshopify.com/admin" % (API_KEY, PASSWORD, SHOP_NAME)
        shopify.ShopifyResource.set_site(shop_url)
        shop = shopify.Shop.current()
        print "Connected to " + shop.name +  " successfully."
         
        return shop
    
    # #
    # # Function to insert notes to a specific order
    # # Pass in your note, your order number
    # #
    def insertNoteToOrder(noteStr, orderNum):
        
        order = shopify.Order()
        orderObj = order.find(orderNum)         
        orderObj.note = noteStr
        #result = orderObj.save()  CDC - uncomment this    *** 
        
    # #
    # #Test methods.
    # #
    def stringToJSONObjectTestMethod(str):
        myJSON = json.loads(str.replace("\'", '"'))
        return myJSON
        
    def parametersContainerTestMethod():
         # Testing global parameters and Shopify parameters - getters and setters work as of 2017-04-11
        myParamCont = GlobalParametersContainer()
        myParamCont.setAmazoncloudpassword("testValue")
        print myParamCont.getAmazoncloudpassword()
        
        myParamContSHPFY = SHPFYParametersContainer()
        myParamContSHPFY.setTime(time.strftime("%d/%m/20%y/ %H:%M:%S"))
        print myParamContSHPFY.getTime()
        
        # test a list
        list1 = [1, 2, 3, 4, 5 ];
        myParamCont.setSesreceiveremailaddress(list1)
        print myParamCont.getSesreceiveremailaddress()
        myParamContSHPFY.setSesreceiveremailaddress(list1)
        print myParamContSHPFY.getSesreceiveremailaddress() 

   # testing code...
#     sampleStringInput1 = "[{\"field1\": 1,\"field2\": 2}, {\"field1\": 11,\"field2\": 22}]" 
#     realWorldSampleString ="{\"customername\":\"COSMO\","    + "\"sessionid\":\"91879945\","    + "\"password\":\"Cosmo_2016$\","    + "\"username\":\"vlogistics@cosmomusic.ca\","    + "\"apiurl\": \"cpanel.cosmomusic.ca\","    + "\"source\" : \"AMASC\","    + "\"path\" : \"/Test/orders/\","    + "\"bucketname\":\"middlewareworkflow\","    + "\"workflowname\":\"cosmoFtpTest\""    + "}";
#      
#     #ParametersContainerTestMethod()    
#     myJSONObj = stringToJSONObjectTestMethod(sampleStringInput1)
#     myJSONObj2 = stringToJSONObjectTestMethod(realWorldSampleString)
#     
#     outputString = "Customer Name: " + myJSONObj2['customername'] + " Workflow Name: " + myJSONObj2['workflowname']
#     print outputString
#     #Product Test Code
#     product = shopify.Product.find(135373675)
#     variant = shopify.Variant.find(309184511, product_id=135373675)  # use your ids      
#     variant.price = 9.99      
#     result= variant.save()
#     print result

    myProdID = 153
    myVarID = 111
    currTime = time.strftime('%Y-%m-%d %H:%M:%S')
        
    #***PROGRAM START.    

    shop = connectToSHPFYStore()
    
    # limit hardcoded to 250 - concatenate page_size variable in if needed...
# #VL WebStore
#     getCountCustomerStatement = "https://e9530acd78fa8c4c527ccc092e5638e7:b3ae8cb4afc28c16678dfda7581f893e@vl-test-store.myshopify.com/admin/customers/count.json"
#     getCountProductStatement = "https://e9530acd78fa8c4c527ccc092e5638e7:b3ae8cb4afc28c16678dfda7581f893e@vl-test-store.myshopify.com/admin/products/count.json"
#     getCountOrderStatement = "https://e9530acd78fa8c4c527ccc092e5638e7:b3ae8cb4afc28c16678dfda7581f893e@vl-test-store.myshopify.com/admin/orders/count.json"
#     getCustomersStatement = "https://e9530acd78fa8c4c527ccc092e5638e7:b3ae8cb4afc28c16678dfda7581f893e@vl-test-store.myshopify.com/admin/customers.json?&limit=9&page="
#     getProductsStatement = "https://e9530acd78fa8c4c527ccc092e5638e7:b3ae8cb4afc28c16678dfda7581f893e@vl-test-store.myshopify.com/admin/products.json?&limit=9&page="
#     getOrdersStatement = "https://e9530acd78fa8c4c527ccc092e5638e7:b3ae8cb4afc28c16678dfda7581f893e@vl-test-store.myshopify.com/admin/orders.json?&limit=9&page="
    
       # Unique Salon Concepts
    getCountCustomerStatement = "https://073f51bd8cc118dea2003af3d785466a:e906a4d2e38741bb279d3f152c9f0f99@unique-salon-concepts.myshopify.com/admin/customers/count.json"
    getCountProductStatement = "https://073f51bd8cc118dea2003af3d785466a:e906a4d2e38741bb279d3f152c9f0f99@unique-salon-concepts.myshopify.com/admin/products/count.json"
    getCountOrderStatement = "https://073f51bd8cc118dea2003af3d785466a:e906a4d2e38741bb279d3f152c9f0f99@unique-salon-concepts.myshopify.com/admin/orders/count.json"
    getCustomersStatement = "https://073f51bd8cc118dea2003af3d785466a:e906a4d2e38741bb279d3f152c9f0f99@unique-salon-concepts.myshopify.com/admin/customers.json?&limit=250&page="
    getProductsStatement = "https://073f51bd8cc118dea2003af3d785466a:e906a4d2e38741bb279d3f152c9f0f99@unique-salon-concepts.myshopify.com/admin/products.json?&limit=250&page="
    getOrdersStatement = "https://073f51bd8cc118dea2003af3d785466a:e906a4d2e38741bb279d3f152c9f0f99@unique-salon-concepts.myshopify.com/admin/orders.json?&limit=250&page="
              
    orderNum = 4813161426
    noteStr = "VLOMNI-Order-" + str(orderNum) + " successfully processed at " + currTime    
    # insertNoteToOrder(noteStr, orderNum) ****CDC use this method in the future for adding notes!     
    
    
    # Dev DB     
    # mydbc = DatabaseConnector("VLWebService", "sqladmin",  "VL1omni!$!", "vl-aurora1.cdpcz0fpc2s4.us-east-1.rds.amazonaws.com")
    # my local DB
    mydbc = DatabaseConnector("sys", "root", "chrsqlR1_", "127.0.0.1")
    mydbc.connectToDatabase()
   
    myRequestCust = requests.get(getCountCustomerStatement)
    reqObj1 = json.loads(myRequestCust.content)
    totalNumCustomers = reqObj1['count']
    current_page = 1
    page_size = 250.0  # 250 is max
    custPages = math.ceil(totalNumCustomers / page_size)
    print str(totalNumCustomers) + " customers (" + str(custPages) + " pages)..."
     
    myRequestProd = requests.get(getCountProductStatement)
    reqObj2 = json.loads(myRequestProd.content)
    totalNumProducts = reqObj2['count']
    prodPages = math.ceil(totalNumProducts / page_size)
    print str(totalNumProducts) + " products (" + str(prodPages) + " pages)..." 
      
    myRequestOrd = requests.get(getCountOrderStatement)
    reqObj3 = json.loads(myRequestOrd.content)
    totalNumOrders = reqObj3['count']
    ordPages = math.ceil(totalNumOrders / page_size)
    print str(totalNumOrders) + " orders (" + str(ordPages) + " pages)..."

    #***CUSTOMERS
    myCustomers = []    
    # Must do this while loop to ensure we are not throttled by Shopify's limit
    while(current_page <= custPages):  # note that current page starts at 1 NOT 0 so therefore <=
        response = requests.get(getCustomersStatement + str(current_page) + "")
        tempJSON = simplejson.loads(str(response.content))
        myCustomers += tempJSON['customers']
        current_page += 1  
        time.sleep(1.5)
    
    print ("generated list of all customers.")    
    
    #***PRODUCTS
    current_page = 1  # reset this for next loop
    myProducts = []     
    # Must do this while loop to ensure we are not throttled by Shopify's limit
    while(current_page <= prodPages):  # note that current page starts at 1 NOT 0 so therefore <=
        response = requests.get(getProductsStatement + str(current_page) + "")
        tempJSON = simplejson.loads(str(response.content))
        myProducts += tempJSON['products']
        current_page += 1 
        time.sleep(1.5)
    
    print ("generated list of all products.")
        
    #***ORDERS
    current_page = 1  # reset this for next loop    
    myOrders = []     
    # Must do this while loop to ensure we are not throttled by Shopify's limit
    while(current_page <= ordPages):  # note that current page starts at 1 NOT 0 so therefore <=
        response = requests.get(getOrdersStatement + str(current_page) + "")
        tempJSON = simplejson.loads(str(response.content))
        myOrders += tempJSON['orders']
        current_page += 1 
        time.sleep(1.5)
    
    print ("generated list of all orders.")

    # //code to get information within order...
#     myLineItems = []
#     for lineItem in range(0, len(myOrders)):
#         myLineItems.append(myOrders[lineItem]['line_items'])
#     myOrderProducts = []
#     for prod in range(0,len(myLineItems)):
#         myOrderProducts.append(myLineItems[prod])
#     myOrderCustomers = []
#     for cust in range(0,len(myOrders)):
#         myOrderCustomers.append(myOrders[cust]['customer'])
    
    currTime = time.strftime('%Y-%m-%d %H:%M:%S')  # taking this out of loop so seconds of POKey remain the same for each batch
    currTimeNoSpace = time.strftime('%Y%m%d%H%M%S')
    for ord in range(0, len(myOrders)):
        counter = 0  # reset counter every order.
                
        POKey = "VLOMNI" + str(myOrders[ord]['checkout_id']) + currTimeNoSpace  # cdc  VLOMNI is hardcoded for now...
         
         
        # Problems - due to numerous attribute errors (some properties don't exist) keyerrors, etc, I have to do multiple try catch statements.
        # If checks to see if null still generate exceptions
        # nested for loop - for each line_item within the current order, insert to WebOrderDetail
        for myLineItem in range(0, len(myOrders[ord]['line_items'])):            
            counter += 1
            currLineItem = myOrders[ord]['line_items'][myLineItem]
            if(len(currLineItem['tax_lines']) > 1):
                Tax1 = str(currLineItem['tax_lines'][0]['price'])
                Tax2 = str(currLineItem['tax_lines'][1]['price'])
            elif(len(currLineItem['tax_lines']) == 1):
                Tax1 = str(currLineItem['tax_lines'][0]['price'])
                Tax2 = "0"
            else:
                Tax1 = "0"
                Tax2 = "0"        
               
            try:    
                UOM = currLineItem['variant_title'].replace("'", "")
            except:
                UOM = "None"
            try:
                ItemNotes = currLineItem['name'].replace("'", "")  
            except:
                ItemNotes = "None"  
            try:
                myDescription = currLineItem['title'].replace("'", "")  
            except:
                myDescription = "None"  
               
                   
            WODOrderData = ("{ \"WODOrder\" : {\"POKEY\":\"" + POKey 
                            + "\",    \"POLine\":\"" + str(counter) 
                            + "\",    \"Customer_PO_Line\":\"" + str(currLineItem['id']) 
                            + "\",    \"Sequence\":\"" + str(counter) 
                            + "\",    \"OrderQty\":\"" + str(currLineItem['quantity']) 
                            + "\",    \"ShipQty\":\"" + str(currLineItem['quantity']) 
                            + "\",    \"UOM\":\"" + UOM 
                            + "\",    \"UnitPrice\":\"" + currLineItem['price'] 
                            + "\",    \"SKU\":\"" + currLineItem['sku'] 
                            + "\",    \"CustomerSKU\":\"" + str(currLineItem['id']) 
                            + "\",    \"UPC\":\"" + str(currLineItem['product_id']) 
                            + "\",    \"Description\":\"" + myDescription 
                            + "\",    \"ItemNotes\":\"" + ItemNotes 
                            + "\",    \"Tax\":\"" + Tax1 
                            + "\",    \"Tax2\":\"" + Tax2 
                            + "\" } } ")    
            print WODOrderData 
            generateInsertOrderIntoDB(WODOrderData, "WOD") 
                           
        # end nested for loop
          
        try:
            currBillingAdd = myOrders[ord]['billing_address']
            try:
                myName = currBillingAdd['name'].replace("'", "")
            except:
                myName = "None"
            try:
                myCompany = currBillingAdd['company'].replace("'", "")
            except:
                myCompany = "None"
                   
            WOAOrderData1 = ("{ \"WOAOrder\" : {\"AddressCode\":\"" + str(myOrders[ord]['id']) 
                             + "\",    \"AddressType\":\"" + "BT" 
                             + "\",    \"Name\":\"" + myName 
                             + "\",    \"Address1\":\"" + str(currBillingAdd['address1']) 
                             + "\",    \"Address2\":\"" + str(currBillingAdd['address2']) 
                             + "\",    \"City\":\"" + str(currBillingAdd['city']) 
                             + "\",    \"Province\":\"" + str(currBillingAdd['province_code']) 
                             + "\",    \"PostalCode\":\"" + str(currBillingAdd['zip']) 
                             + "\",    \"Country\":\"" + str(currBillingAdd['country_code']) 
                             + "\",    \"Phone\":\"" + str(currBillingAdd['phone']) 
                             + "\",    \"AcctCode\":\"" + str(myOrders[ord]['customer']['id']) 
                             + "\",    \"Company\":\"" + myCompany 
                             + "\" } } ")    
            print WOAOrderData1 
            generateInsertOrderIntoDB(WOAOrderData1, "WOA")
        except KeyError:
            print "There is no billing address for this order id: " + str(myOrders[ord]['id']) 
           
        try:    
            currShippingAdd = myOrders[ord]['shipping_address']
            try:
                myName = currShippingAdd['name'].replace("'", "")
            except:
                myName = "None"
            try:
                myCompany = currShippingAdd['company'].replace("'", "")
            except:
                myCompany = "None"
                   
            WOAOrderData2 = ("{ \"WOAOrder\" : {\"AddressCode\":\"" + str(myOrders[ord]['id']) 
            + "\",    \"AddressType\":\"" + "ST" 
            + "\",    \"Name\":\"" + myName 
            + "\",    \"Address1\":\"" + str(currShippingAdd['address1']) 
            + "\",    \"Address2\":\"" + str(currShippingAdd['address2']) 
            + "\",    \"City\":\"" + str(currShippingAdd['city']) 
            + "\",    \"Province\":\"" + str(currShippingAdd['province_code']) 
            + "\",    \"PostalCode\":\"" + str(currShippingAdd['zip']) 
            + "\",    \"Country\":\"" + str(currShippingAdd['country_code']) 
            + "\",    \"Phone\":\"" + str(currShippingAdd['phone']) 
            + "\",    \"AcctCode\":\"" + str(myOrders[ord]['customer']['id']) 
            + "\",    \"Company\":\"" + myCompany 
            + "\" } } ")    
            print WOAOrderData2 
            generateInsertOrderIntoDB(WOAOrderData2, "WOA")
        except KeyError:
            print "There is no shipping address for this order id: " + str(myOrders[ord]['id']) 
           
            myShipMethod = ""
            myShipMethodDesc = ""
            myShippingCost = ""
        try:
            myShipMethod = str(myOrders[ord]['shipping_lines'][0]['title'])
            myShipMethodDesc = str(myOrders[ord]['shipping_lines'][0]['title'])
            myShippingCost = str(myOrders[ord]['shipping_lines'][0]['price'])
        except:
            myShipMethod = "No Shipping Lines present for this order"
            myShipMethodDesc = "No Shipping Lines present for this order"
            myShippingCost = "0"
        var_discount = float(myOrders[ord]['total_discounts']) 
        var_subtotal = float(myOrders[ord]['subtotal_price'])
        orderNotes = str(myOrders[ord]['note']).replace('<p>', '').replace('"', "").replace("'", '').replace('</p>', '').replace('\n', '').replace('\r', '').replace('\u', '').replace('\t', '').replace('\b', '')
           
        PercentageDiscount = "%0.2f" % (abs(var_discount) / (var_subtotal + abs(var_discount)))    
           
        myFirstName = "None"
        myLastName = "None"  # default values to protect against keyerrors exceptions 
        try:
            if(myOrders[ord]['customer']['first_name'] is not None):
                myFirstName = str(myOrders[ord]['customer']['first_name']).replace("'", "")
            if(myOrders[ord]['customer']['last_name'] is not None):
                myLastName = myOrders[ord]['customer']['last_name'].replace("'", "")
        except KeyError:
            print "No customer property for this order?? -> " + str(myOrders[ord]['id'])
        try:
            # CDC For transaction fields: You must do a get /admin/orders/#{id}/transactions/#{id}.json  ...you may hit API call limits
            WOHOrderData = ("{ \"WOHOrder\" : { \"WebOrderNumber\":\"" + str(myOrders[ord]['id']) 
            + "\",    \"WebOrderID\":\"" + str(myOrders[ord]['checkout_id']) 
            + "\",    \"OrderDate\":\"" + str(myOrders[ord]['created_at']) 
            + "\",    \"CreatedDate\":\"" + currTime 
            + "\",    \"Fulfillment_Status\":\"" + str(myOrders[ord]['fulfillment_status']) 
            + "\",    \"FirstName\":\"" + myFirstName 
            + "\",    \"LastName\":\"" + myLastName 
            + "\",    \"Webstore\":\"" + "SHOPIFY"  
            + "\",    \"Currency\":\"" + str(myOrders[ord]['currency']) 
            + "\",    \"ShipTo\":\"" + str(myOrders[ord]['id']) 
            + "\",    \"ShipMethod\":\"" + myShipMethod 
            + "\",    \"ShipMethodDesc\":\"" + myShipMethodDesc 
            + "\",    \"ShippingCost\":\"" + myShippingCost  
            + "\",    \"IPAddress\":\"" + str(myOrders[ord]['browser_ip']) 
            + "\",    \"OrderNotes\":\"" + orderNotes 
            + "\",    \"Subtotal\":\"" + str(myOrders[ord]['subtotal_price']) 
            + "\",    \"Discount\":\"" + str(myOrders[ord]['total_discounts']) 
            + "\",    \"Tax\":\"" + str(myOrders[ord]['total_tax']) 
            + "\",    \"GrandTotal\":\"" + str(myOrders[ord]['total_price']) 
            + "\",    \"PercentageDiscount\":\"" + PercentageDiscount 
            + "\",    \"PaymentMethod\":\"" + str(myOrders[ord]['gateway']) 
            + "\",    \"BuyerEmail\":\"" + str(myOrders[ord]['email']) 
            + "\" } } ")  # BuyerEmail not currently in table, though  
            print WOHOrderData 
            generateInsertOrderIntoDB(WOHOrderData, "WOH") 
        except KeyError:
            print "failed to insert to WebOrderHeader id -> " + str(myOrders[ord]['id'])       
           
          
            
           
    # foreach customer, generate a JSON object and insert to database
       
    for cust in range(0, len(myCustomers)):
        try:
            # how to handle lists in product - iterate through them and append to a string. Then put that string in the data
            # only id is int
            # there is only ONE default address so no need for the iteration
            try:
                addressFirstName = myCustomers[cust]['default_address']['first_name']
                addressFirstName = addressFirstName.encode('utf8')
            except:
                addressFirstName = "None"
            try: 
                addressLastName = myCustomers[cust]['default_address']['last_name']
                addressLastName = addressLastName.encode('utf8')
            except:
                addressLastName = "None"
            try:
                addressName = myCustomers[cust]['default_address']['name']
                addressName = addressName.encode('utf8')
            except:
                addressName = "None"
            
            custString = " [" 
            custString = custString + "{ \"""address1\":\"" + str(myCustomers[cust]['default_address']['address1']) + "\" , "
            custString = custString + " \"address2\":\"" + str(myCustomers[cust]['default_address']['address2']) + "\" , "
            custString = custString + " \"city\":\"" + str(myCustomers[cust]['default_address']['city']) + "\" , "
            custString = custString + " \"company\":\"" + str(myCustomers[cust]['default_address']['company']) + "\" , "
            custString = custString + " \"country\":\"" + str(myCustomers[cust]['default_address']['country']) + "\" , "
            custString = custString + " \"country_code\":\"" + str(myCustomers[cust]['default_address']['country_code']) + "\" , "
            custString = custString + " \"country_name\":\"" + str(myCustomers[cust]['default_address']['country_name']) + "\" , "
            custString = custString + " \"default\":\"" + str(myCustomers[cust]['default_address']['default']) + "\" , "
            custString = custString + " \"first_name\":\"" + addressFirstName + "\" , "
            custString = custString + " \"id\": " + str(myCustomers[cust]['default_address']['id']) + " , "
            custString = custString + " \"last_name\":\"" + addressLastName + "\" , "
            custString = custString + " \"name\":\"" + addressName + "\" , "
            custString = custString + " \"phone\":\"" + str(myCustomers[cust]['default_address']['phone']) + "\" , "
            custString = custString + " \"province\":\"" + str(myCustomers[cust]['default_address']['province']) + "\" , "
            custString = custString + " \"province_code\":\"" + str(myCustomers[cust]['default_address']['province_code']) + "\" , "
            custString = custString + " \"zip\":\"" + str(myCustomers[cust]['default_address']['zip']) + "\" } ]"  
    
        except KeyError, e:
            print 'default_address doesnt exist for this customer - %s %s ' % (myCustomers[cust]['first_name'], myCustomers[cust]['last_name']) 
            custString = "[]" 
    
        try:
            custFirstName = myCustomers[cust]['first_name']
            custFirstName = custFirstName.encode('utf8')
        except:
            custFirstName = "None"
        
        try:
            custLastName = myCustomers[cust]['last_name']
            custLastName = custLastName.encode('utf8')
        except:
            custLastName = "None"
         
         
        customerData = ("{ \"customer\" : { \"accepts_marketing\":\"" + str(myCustomers[cust]['accepts_marketing']) 
        + "\",        \"created_at\":\"" + str(myCustomers[cust]['created_at']) 
        + "\", "    " \"default_address\": " + custString  
        + ", "      " \"email\":\"" + str(myCustomers[cust]['email']) 
        + "\", "    " \"first_name\":\"" + custFirstName.replace("'", "")
        + "\", "    " \"id\": " + str(myCustomers[cust]['id']) 
        + ", "      " \"last_name\":\"" + custLastName.replace("'", "")
        + "\", "    " \"last_order_id\":\"" + str(myCustomers[cust]['last_order_id']) 
        + "\", "    " \"last_order_name\":\"" + str(myCustomers[cust]['last_order_name']) 
        + "\", "    " \"multipass_identifier\":\"" + str(myCustomers[cust]['multipass_identifier']) 
        + "\", "    " \"note\":\"" + str(myCustomers[cust]['note']) 
        + "\", "    " \"orders_count\": " + str(myCustomers[cust]['orders_count']) 
        + ", "      " \"phone\":\"" + str(myCustomers[cust]['phone']) 
        + "\", "    " \"state\":\"" + str(myCustomers[cust]['state']) 
        + "\", "    " \"tags\":\"" + str(myCustomers[cust]['tags']) 
        + "\", "    " \"tax_exempt\":\"" + str(myCustomers[cust]['tax_exempt']) 
        + "\", "    " \"total_spent\": " + str(myCustomers[cust]['total_spent']) 
        + ", "      " \"updated_at\":\"" + str(myCustomers[cust]['updated_at']) 
        + "\", "    " \"verified_email\":\"" + str(myCustomers[cust]['verified_email']) 
        + "\" } } ")    
        print customerData
        generateInsertCustomerIntoDB(customerData) 
          
 
        # doesn't work and testing indicates that executemany() isn't significantly faster anyways
         
#         insertDef = """INSERT INTO Customer (email, first_name, last_name, date_created, date_updated, inserted) VALUES (%s, %s, %s, %s, %s, %s) """
#         mydbc.massInsertToDB(insertDef, InsertStatementCompilation)
         
    for prod in range(0, len(myProducts)):  
        
        product = myProducts[prod] 
        
                            
        # CDC remove the if else checks for None if you need more performance
        
        imageList = product['images']
        if len(imageList) > 0:
            imageString = " ["
            for myImage in range(0, len(imageList)): 
                if myImage == len(imageList) - 1:   
                    if imageList[myImage]['id'] is not None:
                        imageString = imageString + "{ \"id\": " + str(imageList[myImage]['id']) + " } ]"
                    else: 
                        imageString = imageString + "{ \"id\": -1 } ]"
                else:
                    if imageList[myImage]['id'] is not None:
                        imageString = imageString + "{ \"id\": " + str(imageList[myImage]['id']) + " }, "
                    else:    
                        imageString = imageString + "{ \"id\": -1 }, "            
        else:
            imageString = "[]"                    
             
        optionList = product['options'] 
        optionString = " ["
        if len(optionList) > 0:        
            for myOption in range(0, len(optionList)):    
                if myOption == len(optionList) - 1:
                    if optionList[myOption]['id'] is not None:
                        optionString = optionString + "{ \"id\": " + str(optionList[myOption]['id']) + " } ]" 
                    else:   
                        optionString = optionString + "{ \"id\": -1  } ]" 
                else:
                    if optionList[myOption]['id'] is not None:
                        optionString = optionString + "{ \"id\": " + str(optionList[myOption]['id']) + " }, "             
                    else:
                        optionString = optionString + "{ \"id\": -1  }, "
           
        #***PROBLEM - some products don't have all of the fields so I have to do an if check to see if it's none. 
        # if none, treat it as a string for storage or else you get an error. This is necessary for integer fields only since strings can handle it anyways
        variantString = " ["
        variantList = product['variants']
        for myVariant in range(0, len(variantList)): 
            # #if it's the last variant in the list
            if myVariant == len(variantList) - 1:
                variantString = variantString + "{ \"barcode\":\"" + str(variantList[myVariant]['barcode']) + "\" , "
                variantString = variantString + " \"compare_at_price\":\"" + str(variantList[myVariant]['compare_at_price']) + "\" , "
                variantString = variantString + " \"created_at\":\"" + str(variantList[myVariant]['created_at']) + "\" , "
                variantString = variantString + " \"fulfillment_service\":\"" + str(variantList[myVariant]['fulfillment_service']) + "\" , "
                 
                if variantList[myVariant]['grams'] is not None:
                    variantString = variantString + " \"grams\": " + str(variantList[myVariant]['grams']) + " , "
                else:
                    variantString = variantString + " \"grams\":\"" + str(variantList[myVariant]['grams']) + "\" , "
                 
                if variantList[myVariant]['id'] is not None:
                    variantString = variantString + " \"id\": " + str(variantList[myVariant]['id']) + " , "
                else:
                    variantString = variantString + " \"id\":\"" + str(variantList[myVariant]['id']) + "\" , "
                 
                if variantList[myVariant]['image_id'] is not None:
                    variantString = variantString + " \"image_id\": " + str(variantList[myVariant]['image_id']) + " , "
                else:
                    variantString = variantString + " \"image_id\":\"" + str(variantList[myVariant]['image_id']) + "\" , "
                variantString = variantString + " \"inventory_management\":\"" + str(variantList[myVariant]['inventory_management']) + "\" , "
                variantString = variantString + " \"inventory_policy\":\"" + str(variantList[myVariant]['inventory_policy']) + "\" , "
                 
                if variantList[myVariant]['inventory_quantity'] is not None:
                    variantString = variantString + " \"inventory_quantity\": " + str(variantList[myVariant]['inventory_quantity']) + " , "
                else: 
                    variantString = variantString + " \"inventory_quantity\":\"" + str(variantList[myVariant]['inventory_quantity']) + "\" , "
                
                if variantList[myVariant]['old_inventory_quantity'] is not None:
                    variantString = variantString + " \"old_inventory_quantity\": " + str(variantList[myVariant]['old_inventory_quantity']) + " , "
                else:
                    variantString = variantString + " \"old_inventory_quantity\":\"" + str(variantList[myVariant]['old_inventory_quantity']) + "\" , "
                  
                option1 = str(variantList[myVariant]['option1'])
                option2 = str(variantList[myVariant]['option2']) 
                option3 = str(variantList[myVariant]['option3'])  
                variantString = variantString + " \"option1\":\"" + option1.replace("'", "") + "\" , "
                variantString = variantString + " \"option2\":\"" + option2.replace("'", "") + "\" , "
                variantString = variantString + " \"option3\":\"" + option3.replace("'", "") + "\" , "
                 
                if variantList[myVariant]['position'] is not None:
                    variantString = variantString + " \"position\": " + str(variantList[myVariant]['position']) + " , "
                else:
                    variantString = variantString + " \"position\":\"" + str(variantList[myVariant]['position']) + "\" , "
                variantString = variantString + " \"price\":\"" + str(variantList[myVariant]['price']) + "\" , "  
                variantString = variantString + " \"requires_shipping\":\"" + str(variantList[myVariant]['requires_shipping']) + "\" , "
                variantString = variantString + " \"sku\":\"" + str(variantList[myVariant]['sku']) + "\" , " 
                variantString = variantString + " \"taxable\":\"" + str(variantList[myVariant]['taxable']) + "\" , " 
                
                title = str(variantList[myVariant]['title'])                
                variantString = variantString + " \"title\":\"" + title.replace("'", "") + "\" , " 
                
                variantString = variantString + " \"updated_at\":\"" + str(variantList[myVariant]['updated_at']) + "\" , " 
                 
                if variantList[myVariant]['weight'] is not None:
                    variantString = variantString + " \"weight\": " + str(variantList[myVariant]['weight']) + " , "  
                else:
                    variantString = variantString + " \"weight\":\"" + str(variantList[myVariant]['weight']) + "\" , "       
                variantString = variantString + " \"weight_unit\":\"" + str(variantList[myVariant]['weight_unit']) + "\" } ] " 
            else:      
                variantString = variantString + "{ \"barcode\":\"" + str(variantList[myVariant]['barcode']) + "\" , "
                variantString = variantString + " \"compare_at_price\":\"" + str(variantList[myVariant]['compare_at_price']) + "\" , "
                variantString = variantString + " \"created_at\":\"" + str(variantList[myVariant]['created_at']) + "\" , "
                variantString = variantString + " \"fulfillment_service\":\"" + str(variantList[myVariant]['fulfillment_service']) + "\" , "
                 
                if variantList[myVariant]['grams'] is not None:
                    variantString = variantString + " \"grams\": " + str(variantList[myVariant]['grams']) + " , "
                else:
                    variantString = variantString + " \"grams\":\"" + str(variantList[myVariant]['grams']) + "\" , "
                 
                if variantList[myVariant]['id'] is not None:
                    variantString = variantString + " \"id\": " + str(variantList[myVariant]['id']) + " , "
                else:
                    variantString = variantString + " \"id\":\"" + str(variantList[myVariant]['id']) + "\" , "
                 
                if variantList[myVariant]['image_id'] is not None:
                    variantString = variantString + " \"image_id\": " + str(variantList[myVariant]['image_id']) + " , "
                else:
                    variantString = variantString + " \"image_id\":\"" + str(variantList[myVariant]['image_id']) + "\" , "
                variantString = variantString + " \"inventory_management\":\"" + str(variantList[myVariant]['inventory_management']) + "\" , "
                variantString = variantString + " \"inventory_policy\":\"" + str(variantList[myVariant]['inventory_policy']) + "\" , "
                 
                if variantList[myVariant]['inventory_quantity'] is not None:
                    variantString = variantString + " \"inventory_quantity\": " + str(variantList[myVariant]['inventory_quantity']) + " , "
                else: 
                    variantString = variantString + " \"inventory_quantity\":\"" + str(variantList[myVariant]['inventory_quantity']) + "\" , "
                
                if variantList[myVariant]['old_inventory_quantity'] is not None:
                    variantString = variantString + " \"old_inventory_quantity\": " + str(variantList[myVariant]['old_inventory_quantity']) + " , "
                else:
                    variantString = variantString + " \"old_inventory_quantity\":\"" + str(variantList[myVariant]['old_inventory_quantity']) + "\" , "
                    
                option1 = str(variantList[myVariant]['option1'])
                option2 = str(variantList[myVariant]['option2']) 
                option3 = str(variantList[myVariant]['option3'])  
                variantString = variantString + " \"option1\":\"" + option1.replace("'", "") + "\" , "
                variantString = variantString + " \"option2\":\"" + option2.replace("'", "") + "\" , "
                variantString = variantString + " \"option3\":\"" + option3.replace("'", "") + "\" , "
                 
                if variantList[myVariant]['position'] is not None:
                    variantString = variantString + " \"position\": " + str(variantList[myVariant]['position']) + " , "
                else:
                    variantString = variantString + " \"position\":\"" + str(variantList[myVariant]['position']) + "\" , "
                variantString = variantString + " \"price\":\"" + str(variantList[myVariant]['price']) + "\" , " 
                variantString = variantString + " \"requires_shipping\":\"" + str(variantList[myVariant]['requires_shipping']) + "\" , "
                variantString = variantString + " \"sku\":\"" + str(variantList[myVariant]['sku']) + "\" , " 
                variantString = variantString + " \"taxable\":\"" + str(variantList[myVariant]['taxable']) + "\" , " 
                variantString = variantString + " \"title\":\"" + str(variantList[myVariant]['title']) + "\" , " 
                variantString = variantString + " \"updated_at\":\"" + str(variantList[myVariant]['updated_at']) + "\" , " 
                 
                if variantList[myVariant]['weight'] is not None:
                    variantString = variantString + " \"weight\": " + str(variantList[myVariant]['weight']) + " , "  
                else:
                    variantString = variantString + " \"weight\":\"" + str(variantList[myVariant]['weight']) + "\" , "       
                variantString = variantString + " \"weight_unit\":\"" + str(variantList[myVariant]['weight_unit']) + "\" }, " 
            
            if(product['body_html'] is not None):     
                body_htmlString = product['body_html'].replace('-', ' ').replace('<p>', '').replace('"', "").replace("'", '').replace('</p>', '').replace('\n', '').replace('\r', '').replace('\u', '').replace('\t', '').replace('\b', '').replace('&bull', '')
            else:   
                body_htmlString = ""         
        prodTitle = product['title'].replace('"', '')
        productData = ("{ \"product\": { \"body_html\":\"" + body_htmlString 
        + "\",    \"created_at\":\"" + product['created_at'] 
        + "\",    \"handle\":\"" + product['handle'] 
        + "\",    \"id\":\"" + str(product['id']) 
        + "\",    \"images\":" + imageString 
        + ",      \"options\": " + optionString 
        + ",      \"product_type\":\"" + product['product_type'] 
        + "\",    \"published_at\":\"" + product['published_at'] 
        + "\",    \"published_scope\":\"" + product['published_scope'] 
        + "\",    \"tags\":\"" + product['tags'] 
        + "\",    \"title\":\"" + prodTitle 
        + "\",    \"updated_at\":\"" + product['updated_at'] 
        + "\",    \"variants\":" + variantString 
        + ",      \"vendor\":\"" + product['vendor'] 
        + "\"} }")
        print productData 
        generateInsertProductIntoDB(productData)
    
       
#     tempMethod()
#     t= Timer(lambda: tempMethod())
#     print t.timeit(number=1)

if __name__ == '__main__': main()
   


